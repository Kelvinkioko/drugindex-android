package com.dip.drugindex.authentication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/27/2018.
 */

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    @BindView(R.id.asu_fname) EditText _fname;
    @BindView(R.id.asu_lname) EditText _lname;
    @BindView(R.id.asu_email) EditText _email;
    @BindView(R.id.asu_password) EditText _password;
    @BindView(R.id.asu_confpass) EditText _confpass;
    @BindView(R.id.asu_signin) TextView signin;
    @BindView(R.id.asu_signup) TextView signup;

    @BindView(R.id.asu_termsc) TextView _terms;

    @BindView(R.id.card_normal) CardView card_normal;
    @BindView(R.id.card_panic) CardView card_panic;

    @BindView(R.id.normal_message) TextView normal_message;
    @BindView(R.id.panic_message) TextView panic_message;
    public RadioButton _catButton;

    //    Animation
    public Animation slideIn, slideOut;

    //Timer Delay
    private static int DIALOG_TIME_OUT = 2000;

    private ProgressDialog pDialog;
    public String fname, lname, email, password, role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        Typeface tf =   Typeface.createFromAsset(getAssets(),"fonts/regular.OTF");
        _fname.setTypeface(tf);
        _lname.setTypeface(tf);
        _email.setTypeface(tf);
        _password.setTypeface(tf);
        _confpass.setTypeface(tf);

        slideIn = AnimationUtils.loadAnimation(this, R.anim.dialog_show);
        slideOut = AnimationUtils.loadAnimation(this, R.anim.dialog_dismiss);

        // Progress dialog
        pDialog = new ProgressDialog(SignupActivity.this);
        pDialog.setCancelable(false);

        signup.setOnClickListener(v -> {
            fname = _fname.getText().toString();
            lname = _lname.getText().toString();
            email = _email.getText().toString();
            password = _password.getText().toString();

            if(validate(fname, lname, email, password) == true){
                registerUser(fname + " " + lname, email, password);
            }
        });

        signin.setOnClickListener(v -> startActivity(new Intent(SignupActivity.this, SigninActivity.class)));

        _terms.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignupActivity.this);
            // Get the layout inflater
            LayoutInflater inflater = SignupActivity.this.getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            View dialogView = inflater.inflate(R.layout.dialog_terms, null);
            alertDialogBuilder.setView(dialogView);


            TextView dismiss = dialogView.findViewById(R.id.dt_dismiss);

            // create alert dialog
            final AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            dismiss.setOnClickListener(v1 -> alertDialog.dismiss());
        });
    }

    public boolean validate(String fname, String lname, String email, String password) {
        boolean valid = true;
        if (fname.isEmpty()) {
            _fname.setError("Please enter your first name");
            valid = false;
        } else {
            _fname.setError(null);
        }

        if (lname.isEmpty()) {
            _lname.setError("Please enter your last name");
            valid = false;
        } else {
            _lname.setError(null);
        }

        if (email.isEmpty()) {
            _email.setError("enter a valid email address");
            valid = false;
        } else {
            _email.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 15) {
            _password.setError("between 4 and 15 alphanumeric characters");
            valid = false;
        } else {
            _password.setError(null);
        }

        return valid;
    }

    private void registerUser(final String name, final String email, final String password) {
        pDialog.setMessage("Creating Account ...");
        pDialog.show();

        StringRequest objRequest = new StringRequest(Request.Method.POST, UrlConfig.URL_REGISTER,
                response -> {
                    pDialog.hide();

                    try {
                        JSONObject jObj = new JSONObject(response);
                        Log.e("Signup", jObj.toString());
                        String error = jObj.getString("result");
                        if (error.equals("success")) {
                            Toast.makeText(getApplicationContext(), "User successfully registered!", Toast.LENGTH_LONG).show();

                            Intent ver = new Intent(SignupActivity.this, SigninActivity.class);
                            startActivity(ver);
                            finish();
                        } else {
                            String errorMsg = jObj.getString("message");
                            negativeDisplay(errorMsg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                error -> {}) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        Volley.newRequestQueue(SignupActivity.this).add(objRequest);
    }

    private void negativeDisplay(String message){
        card_panic.setVisibility(View.VISIBLE);
        card_panic.startAnimation(slideIn);
        panic_message.setText(message);

        new Handler().postDelayed(() -> {
            card_panic.setVisibility(View.GONE);
            card_panic.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }

}
