package com.dip.drugindex.authentication;

import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.MainActivity;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.core.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/27/2018.
 */

public class SigninActivity extends AppCompatActivity {

    @BindView(R.id.asi_email) EditText _email;
    @BindView(R.id.asi_password) EditText _password;
    @BindView(R.id.asi_signin) Button signin;
    @BindView(R.id.asi_signup) TextView signup;
    @BindView(R.id.asi_recover) TextView recover;

    @BindView(R.id.card_normal) CardView card_normal;
    @BindView(R.id.card_panic) CardView card_panic;

    @BindView(R.id.normal_message) TextView normal_message;
    @BindView(R.id.panic_message) TextView panic_message;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;

    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private AlertDialog dialog;

    public String email, password;

    public Animation slideIn, slideOut;
    //Timer Delay
    private static int DIALOG_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        Typeface tf =   Typeface.createFromAsset(getAssets(),"fonts/regular.OTF");
        _email.setTypeface(tf);
        _password.setTypeface(tf);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        HashMap<String, String> user;

        // Session manager
        session = new SessionManager(getApplicationContext());

        slideIn = AnimationUtils.loadAnimation(this, R.anim.dialog_show);
        slideOut = AnimationUtils.loadAnimation(this, R.anim.dialog_dismiss);

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            db = new SQLiteHandler(this);
            user = db.getUserDetails();
            // User is already logged in. Take him to main activity
            startActivity(new Intent(SigninActivity.this, MainActivity.class));
            finish();
        }

        // Progress dialog
        pDialog = new ProgressDialog(SigninActivity.this);
        pDialog.setCancelable(false);

        signin.setOnClickListener(v -> {

            email = _email.getText().toString();
            password = _password.getText().toString();

            if(validate(email, password) == true){
                opensaseme(email, password);
            }
        });

        recover.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SigninActivity.this);
            LayoutInflater inflater = SigninActivity.this.getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.dialog_forgot_password, null);
            alertDialogBuilder.setView(dialogView);

            EditText emaildialog = dialogView.findViewById(R.id.dfp_email);
            TextView canceldialog = dialogView.findViewById(R.id.dfp_cancel);
            TextView acceptdialog = dialogView.findViewById(R.id.dfp_ok);

            // create alert dialog
            final AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            canceldialog.setOnClickListener(v12 -> alertDialog.cancel());

            acceptdialog.setOnClickListener(v1 -> {alertDialog.dismiss();});
        });

        signup.setOnClickListener(v -> startActivity(new Intent(SigninActivity.this, SignupActivity.class)));

    }

    public boolean validate(String idnum, String password) {
        boolean valid = true;
        if (idnum.isEmpty()) {
            _email.setError("enter a valid E-mail");
            valid = false;
        } else {
            _email.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 15) {
            _password.setError("between 4 and 15 alphanumeric characters");
            valid = false;
        } else {
            _password.setError(null);
        }

        return valid;
    }

    public void opensaseme(final String email, final String password){

        pDialog.setMessage("Login User ...");
        pDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("email", email);
            params.put("password", password);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_LOGIN, params,
                jObj -> {
                    Log.e(TAG, jObj.toString());
                    pDialog.hide();

                    try {
                        if(!jObj.toString().contains("message")) {
                            String error = jObj.getString("success");
                            if (error.equals("Login successful")) {
                                JSONObject feedObj = jObj.getJSONObject("user");

                                String id = feedObj.getString("id");
                                String name = feedObj.getString("name");
                                String email1 = feedObj.getString("email");
                                String phonenumber = feedObj.getString("phone_number");
                                String profilepic = feedObj.getString("image_url");

                                db.addUser(id, name, email1, phonenumber, profilepic);
                                session.setLogin(true);
                                session.setData(true);
                                session.setNotification(true);
                                startActivity(new Intent(SigninActivity.this, MainActivity.class));
                                finish();
                            }
                        } else {
                            String errorMsg = jObj.getString("message");
                            negativeDisplay(errorMsg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            Toast.makeText(SigninActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            pDialog.hide();
        });

        queue.add(jsonObjReq);
    }

    public void recoverPass(final String email){

        pDialog.setMessage("Please wait ...");
        pDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("email", email);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_RECOVER, params,
                jObj -> {
                    Log.e(TAG, jObj.toString());
                    pDialog.hide();

                    try {
                        String result = jObj.getString("success");
                        if (result.equals("Login successful")) {

                        } else {
                            String errorMsg = jObj.getString("message");
                            Toast.makeText(SigninActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            Toast.makeText(SigninActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            pDialog.hide();
        });

        queue.add(jsonObjReq);
    }

    private void negativeDisplay(String message){
        card_panic.setVisibility(View.VISIBLE);
        card_panic.startAnimation(slideIn);
        panic_message.setText(message);

        new Handler().postDelayed(() -> {
            card_panic.setVisibility(View.GONE);
            card_panic.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }

}
