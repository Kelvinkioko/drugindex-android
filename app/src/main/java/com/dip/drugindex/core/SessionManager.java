package com.dip.drugindex.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
	// LogCat tag
	private static String TAG = SessionManager.class.getSimpleName();

	// Shared Preferences
	SharedPreferences pref;

	Editor editor;
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Shared preferences file name
	private static final String PREF_NAME = "DawaatiLogin";
	
	private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
	private static final String KEY_NOTIFICATION = "notificationStatus";
	private static final String KEY_DATA = "dataStatus";

	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setLogin(boolean isLoggedIn) {
		editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
		editor.commit();
	}

	public void setData(boolean isDatafied) {
		editor.putBoolean(KEY_DATA, isDatafied);
		editor.commit();
	}

	public void setNotification(boolean isNotified) {
		editor.putBoolean(KEY_NOTIFICATION, isNotified);
		editor.commit();
	}
	
	public boolean isLoggedIn(){
		return pref.getBoolean(KEY_IS_LOGGED_IN, false);
	}

	public boolean isNotify(){
		return pref.getBoolean(KEY_NOTIFICATION, false);
	}

	public boolean isData(){
		return pref.getBoolean(KEY_DATA, false);
	}
}
