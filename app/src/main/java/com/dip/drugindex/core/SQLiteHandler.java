/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.dip.drugindex.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

	private static final String TAG = SQLiteHandler.class.getSimpleName();

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "rems";

	// Login table name
	private static final String TABLE_USER = "accounts";
	private static final String TABLE_SEARCH = "search";
	private static final String TABLE_PAYMENT = "payment";
	private static final String TABLE_CART = "cart";
	private static final String TABLE_FAV = "fav";

	// Login Table Columns names
	private static final String KEY_ID = "userid";
	private static final String KEY_NAME = "username";
	private static final String KEY_EMAIL = "useremail";
	private static final String KEY_PHONENUMBER = "userpnumber";
	private static final String KEY_PPIC = "profilepicture";

	public static final String SCH_ID = "id";
	public static final String SCH_TITLE = "title";
	public static final String SCH_DESC = "desc";

	public static final String PAY_TYPE = "type";
	public static final String PAY_KEY = "key";
	public static final String PAY_NAME = "name";
	public static final String PAY_CARD = "card";
	public static final String PAY_EXPIRE = "expire";
	public static final String PAY_CVV = "cvv";

	public static final String CART_ID = "id";
	public static final String CART_NAME = "name";
	public static final String CART_IMAGE = "image";
	public static final String CART_COST = "cost";
	public static final String CART_QUANTITY = "quantity";

	public static final String FAV_ID = "id";
	public static final String FAV_TITLE = "title";
	public static final String FAV_DESC = "desc";
	public static final String FAV_PUBBY = "pubby";
	public static final String FAV_IMAGEURL = "imageurl";
	public static final String FAV_PUBDATE = "date";

	public SQLiteHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
				+ KEY_ID + " TEXT PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_EMAIL + " TEXT UNIQUE,"
				+ KEY_PHONENUMBER + " TEXT," + KEY_PPIC + " TEXT " + ")";
		db.execSQL(CREATE_LOGIN_TABLE);

		String CREATE_SEARCH_TABLE = "CREATE TABLE " + TABLE_SEARCH + "("
				+ SCH_ID + " TEXT," + SCH_TITLE + " TEXT," + SCH_DESC + " TEXT " + ")";
		db.execSQL(CREATE_SEARCH_TABLE);

		String CREATE_PAY_TABLE = "CREATE TABLE " + TABLE_PAYMENT + "("
				+ PAY_TYPE + " TEXT," + PAY_KEY + " TEXT," + PAY_NAME + " TEXT," + PAY_CARD + " TEXT,"
				+ PAY_EXPIRE + " TEXT," + PAY_CVV + " TEXT " + ")";
		db.execSQL(CREATE_PAY_TABLE);

		String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CART + "("
				+ CART_ID + " TEXT," + CART_NAME + " TEXT," + CART_IMAGE + " TEXT," + CART_COST + " TEXT,"
				+ CART_QUANTITY + " TEXT " + ")";
		db.execSQL(CREATE_CART_TABLE);

		String CREATE_FAV_TABLE = "CREATE TABLE " + TABLE_FAV + "("
				+ FAV_ID + " TEXT," + FAV_TITLE + " TEXT," + FAV_DESC + " TEXT," + FAV_PUBBY + " TEXT," + FAV_IMAGEURL + " TEXT,"
				+ FAV_PUBDATE + " TEXT " + ")";
		db.execSQL(CREATE_FAV_TABLE);

		Log.d(TAG, "Database tables created");
	}

	public void onCreateNew(SQLiteDatabase db) {
		String CREATE_PAY_TABLE = "CREATE TABLE " + TABLE_PAYMENT + "("
				+ PAY_TYPE + " TEXT," + PAY_KEY + " TEXT," + PAY_NAME + " TEXT," + PAY_CARD + " TEXT,"
				+ PAY_EXPIRE + " TEXT," + PAY_CVV + " TEXT " + ")";
		db.execSQL(CREATE_PAY_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Create tables again
		onCreateNew(db);
	}

	/**
	 * Storing user details in database
	 * */
	public void addUser(String userID, String username, String useremail, String userpnumber,
						String profilepicture) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ID, userID); // UserID
		values.put(KEY_NAME, username); // Name
		values.put(KEY_EMAIL, useremail); // Email
		values.put(KEY_PHONENUMBER, userpnumber); // phonenumber
		values.put(KEY_PPIC, profilepicture); // ppic

		// Inserting Row
		long id = db.insert(TABLE_USER, null, values);
		db.close(); // Closing database connection

		Log.d(TAG, "New user inserted into sqlite: " + id);
	}
	/**
	 * Storing user details in database
	 * */
	public void updateUser(String userID, String username, String useremail, String userpnumber,
						   String profilepicture) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ID, userID); // UserID
		values.put(KEY_NAME, username); // Name
		values.put(KEY_EMAIL, useremail); // Email
		values.put(KEY_PHONENUMBER, userpnumber); // phonenumber
		values.put(KEY_PPIC, profilepicture); // ppic

		// Inserting Row
		long id = db.update(TABLE_USER, values , KEY_ID + "=" + "'" + userID + "'", null);
		db.close(); // Closing database connection

		Log.d(TAG, "user updated into sqlite: " + id);
	}

	/**
	 * Getting user data from database
	 * */
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		String selectQuery = "SELECT  * FROM " + TABLE_USER;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			user.put("userid", cursor.getString(0));
			user.put("username", cursor.getString(1));
			user.put("useremail", cursor.getString(2));
			user.put("userpnumber", cursor.getString(3));
			user.put("profilepicture", cursor.getString(4));
		}
		cursor.close();
		db.close();
		// return user
		Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

		return user;
	}

	/**
	 * Re crate database Delete all tables and create them again
	 * */
	public void deleteUsers() {
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_USER, null, null);
		db.close();

		Log.d(TAG, "Deleted all user info from sqlite");
	}

	public int getUserTotal(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT * FROM " + TABLE_USER + ";";
		Cursor c = db.rawQuery(sql, null);

		return c.getCount();
	}

	/**
	 * Storing user details in database
	 * */
	public void addSearch(String id, String title, String desc) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SCH_ID, id);
		values.put(SCH_TITLE, title);
		values.put(SCH_DESC, desc);

		// Inserting Row
		long idd = db.insert(TABLE_SEARCH, null, values);
		db.close(); // Closing database connection

		Log.d(TAG, "New user inserted into sqlite: " + idd);
	}

	public Cursor getSearch() {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT * FROM " + TABLE_SEARCH + " ;";
		Cursor c = db.rawQuery(sql, null);
		return c;
	}

	public void deleteSearch() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SEARCH, null, null);
		db.close();
	}

	/**
	 * Storing payment details in database
	 * */
	public void addPay(String type, String key, String name, String card, String expire, String cvv) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(PAY_TYPE, type);
		values.put(PAY_KEY, key);
		values.put(PAY_NAME, name);
		values.put(PAY_CARD, card);
		values.put(PAY_EXPIRE, expire);
		values.put(PAY_CVV, cvv);
		long id = db.insert(TABLE_PAYMENT, null, values);
		db.close();
	}

	/**
	 * Updating payment details in database
	 * */
	public void updatePay(String type, String key, String name, String card, String expire, String cvv) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(PAY_TYPE, type);
		values.put(PAY_KEY, key);
		values.put(PAY_NAME, name);
		values.put(PAY_CARD, card);
		values.put(PAY_EXPIRE, expire);
		values.put(PAY_CVV, cvv);

		long id = db.update(TABLE_PAYMENT, values , PAY_TYPE + "=" + "'" + type + "'", null);
		db.close();
	}

	public Cursor getPay() {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT * FROM " + TABLE_PAYMENT + " ;";
		Cursor c = db.rawQuery(sql, null);
		return c;
	}

	/**
	 * Storing payment details in database
	 * */
	public void addCart(String cartid, String name, String image, String quantity, String cost) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CART_ID, cartid);
		values.put(CART_NAME, name);
		values.put(CART_IMAGE, image);
		values.put(CART_QUANTITY, quantity);
		values.put(CART_COST, cost);

		long id = db.insert(TABLE_CART, null, values);
		db.close();
	}

	/**
	 * Updating payment details in database
	 * */
	public void updateCart(String cartid, String quantity) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CART_QUANTITY, quantity);

		long id = db.update(TABLE_CART, values , CART_ID + "=" + "'" + cartid + "'", null);
		db.close();
	}

	public Cursor getCart() {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT * FROM " + TABLE_CART + " ;";
		Cursor c = db.rawQuery(sql, null);
		return c;
	}

	public boolean deleteCart(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CART, CART_ID + "=" + id, null);
		db.close();
		return true;
	}

	public int getPayable(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT SUM(" + CART_COST + ") FROM " + TABLE_CART + ";";
		Cursor c = db.rawQuery(sql, null);
		int gct = 0;
		if(c.moveToFirst()) {
			gct = c.getInt(0);
		}
		c.close();
		db.close();
		return gct;
	}

	/**
	 * Storing user details in database
	 * */
	public void addFav(String id, String title, String desc, String postBy, String imageurl,
						String postDate) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + TABLE_FAV + "'", null);
		if(cursor.getCount() > 0) {
			ContentValues values = new ContentValues();
			values.put(FAV_ID, id); // id
			values.put(FAV_TITLE, title); // title
			values.put(FAV_DESC, desc); // description
			values.put(FAV_PUBBY, postBy); // post by
			values.put(FAV_IMAGEURL, imageurl); // imageurl
			values.put(FAV_PUBDATE, postDate); // post date

			long idm = db.insert(TABLE_FAV, null, values);
			db.close();
		}else{
			String CREATE_FAV_TABLE = "CREATE TABLE " + TABLE_FAV + "("
					+ FAV_ID + " TEXT," + FAV_TITLE + " TEXT," + FAV_DESC + " TEXT," + FAV_PUBBY + " TEXT," + FAV_IMAGEURL + " TEXT,"
					+ FAV_PUBDATE + " TEXT " + ")";
			db.execSQL(CREATE_FAV_TABLE);

			ContentValues values = new ContentValues();
			values.put(FAV_ID, id); // id
			values.put(FAV_TITLE, title); // title
			values.put(FAV_DESC, desc); // description
			values.put(FAV_PUBBY, postBy); // post by
			values.put(FAV_IMAGEURL, imageurl); // imageurl
			values.put(FAV_PUBDATE, postDate); // post date

			long idm = db.insert(TABLE_FAV, null, values);
			db.close();
		}
	}

	public boolean countFav(){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + TABLE_FAV + "'", null);
		if(cursor.getCount() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public Cursor getFav() {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT * FROM " + TABLE_FAV + " ;";
		Cursor c = db.rawQuery(sql, null);
		return c;
	}

	public int getCountFav(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + TABLE_FAV + "'", null);
		if(cursor.getCount() > 0) {
			String sql = "SELECT * FROM " + TABLE_FAV + " WHERE " + FAV_ID + "='" + id + "';";
			Cursor c = db.rawQuery(sql, null);
			return c.getCount();
		}else{
			return 0;
		}
	}

	public boolean deleteFav(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + TABLE_FAV + "'", null);
		if(cursor.getCount() > 0) {
			db.delete(TABLE_FAV, FAV_ID + "=" + id, null);
			db.close();
			return true;
		}else{
			return false;
		}
	}
}
