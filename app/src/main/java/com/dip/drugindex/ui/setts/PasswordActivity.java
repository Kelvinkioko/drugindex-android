package com.dip.drugindex.ui.setts;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dip.drugindex.R;

import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 5/7/2018.
 */

public class PasswordActivity extends AppCompatActivity {

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Change password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        // Progress dialog
        pDialog = new ProgressDialog(PasswordActivity.this);
        pDialog.setCancelable(false);


    }
}
