package com.dip.drugindex.ui.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.core.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/13/2018.
 */

public class ProfileUpdate extends AppCompatActivity {

    @BindView(R.id.pe_name) EditText pe_name;
    @BindView(R.id.pe_email) EditText pe_email;

    private ProgressDialog pDialog;
    private SQLiteHandler db;

    HashMap<String, String> user;
    public String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Profile Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        // Progress dialog
        pDialog = new ProgressDialog(ProfileUpdate.this);
        pDialog.setCancelable(false);

        db = new SQLiteHandler(this);
        user = db.getUserDetails();
        userid = user.get(userid);

        pe_name.setText(user.get("username"));
        pe_email.setText(user.get("useremail"));

        pe_name.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                String temp_name = pe_name.getText().toString().trim();
                pe_name.setHint(temp_name);
            }else{
                if (pe_name.getText().toString().trim().isEmpty()){
                    pe_name.setText(user.get("username"));
                }
            }
        });

        pe_email.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                String temp_email = pe_email.getText().toString().trim();
                pe_email.setHint(temp_email);
            }
        });

    }

    public void updateProfile(final String str_name) {
        pDialog.setMessage("Updating picture ...");
        pDialog.show();

        StringRequest objRequest = new StringRequest(Request.Method.POST, UrlConfig.URL_UPDATE_PROFILE,
                response -> {
                    pDialog.hide();

                    try {
                        JSONObject jObj = new JSONObject(response);
                        Log.e("TAG", jObj.toString());
                        String error = jObj.getString("result");
                        if (error.equals("success")) {
                            JSONObject fObj = jObj.getJSONObject("user");

                            db.updateUser(userid, fObj.getString("name"), fObj.getString("email"),
                                    fObj.getString("phone_number"), fObj.getString("image_url"));
                            finish();
                            Toast.makeText(this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            String errorMsg = jObj.getString("description");
                            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    VolleyLog.d("TAG", "Error: " + error.getMessage());
                    pDialog.hide();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", str_name);
                params.put("user_id", userid);
                return params;
            }
        };

        Volley.newRequestQueue(this).add(objRequest);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.edit_check:
                updateProfile(pe_name.getText().toString().trim());
                return true;
        }
        return false;
    }
}
