package com.dip.drugindex.ui.drugindex;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.BrandAdapter;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.viewModel.BrandViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.ui.details.DrugDetails;
import com.dip.drugindex.views.RotateLoading;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class DIBrand extends Fragment {

    @BindView(R.id.db_progress_lay) RelativeLayout hap_lay;
    @BindView(R.id.db_lay) RelativeLayout db_lay;
    @BindView(R.id.db_progress) RotateLoading db_progress;

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVbrands) RecyclerView RVbrands;
    @BindView(R.id.db_empty)ImageView empty;

    MaterialSearchView searchView;

    private SQLiteHandler db;
    private int search = 0;

    private BrandAdapter brandAdapter;
    private List<BrandModel> brandModel;
    private BrandViewModel brandViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        brandViewModel = ViewModelProviders.of(this).get(BrandViewModel.class);

        if(brandModel.size() == 0){
            RVbrands.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        brandViewModel.getAllBrand().observe(this, activeModels -> {
            if(activeModels.size() > 0){
                if (brandModel.size() == 0 ||
                        brandModel.size() > activeModels.size() ||
                        brandModel.size() < activeModels.size()) {

                    RVbrands.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    brandModel = activeModels;
                    brandAdapter.setBrands(brandModel);
                }
            }
        });
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.drug_brand, null);
        ButterKnife.bind(this, x);

        searchView = getActivity().findViewById(R.id.search_view);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVbrands.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVbrands, (view1, position) -> {

            TextView ia_id = view1.findViewById(R.id.id_id);
            String str_id = ia_id.getText().toString().trim();
            TextView ia_drug = view1.findViewById(R.id.id_drug);
            String str_drug = ia_drug.getText().toString().trim();
            TextView ia_active = view1.findViewById(R.id.id_manufacturer);
            String str_active = ia_active.getText().toString().trim();

            if(search == 1){
                db.addSearch(str_id, str_drug, str_active);
                search = 0;
            }

            Intent active = new Intent(getActivity(), DrugDetails.class);
            active.putExtra("id", str_id);
            active.putExtra("drug", str_drug);
            active.putExtra("active", str_active);
            startActivity(active);
        }));

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        brandModel = new ArrayList<BrandModel>();

        brandAdapter = new BrandAdapter(getActivity(), brandModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVbrands.setLayoutManager(mLayoutManager);
        RVbrands.setItemAnimator(new DefaultItemAnimator());
        RVbrands.setAdapter(brandAdapter);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
        {
            searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    search = 1;
                    Log.i("onQueryTextChange", query);
                    if(TextUtils.isEmpty(query)){
                        brandAdapter.getFilter().filter("");
                    }else{
                        brandAdapter.getFilter().filter(query.toString());
                    }
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    search = 1;
                    Log.i("onQueryTextSubmit", newText);
                    if(TextUtils.isEmpty(newText)){
                        brandAdapter.getFilter().filter("");
                    }else{
                        brandAdapter.getFilter().filter(newText);
                    }
                    return true;
                }
            });

            searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
                @Override
                public void onSearchViewShown() {
                    //Do some magic
                }

                @Override
                public void onSearchViewClosed() {
                    //Do some magic
                }
            });
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView.setMenuItem(searchItem);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
