package com.dip.drugindex.ui.details;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class DrugDetails  extends AppCompatActivity {

    @BindView(R.id.drug_id) TextView drug_id;
    @BindView(R.id.dd_name) TextView drug_name;

    @BindView(R.id.dd_it) TextView indication_title;
    @BindView(R.id.dd_id) TextView indication_details;
    @BindView(R.id.dd_mt) TextView moa_title;
    @BindView(R.id.dd_md) TextView moa_details;
    @BindView(R.id.dd_dt) TextView interaction_title;
    @BindView(R.id.dd_dd) TextView interaction_details;
    @BindView(R.id.dd_st) TextView side_title;
    @BindView(R.id.dd_sd) TextView side_details;
    @BindView(R.id.dd_pt) TextView pregnancy_title;
    @BindView(R.id.dd_pd) TextView pregnancy_details;
    @BindView(R.id.dd_at) TextView additional_title;
    @BindView(R.id.dd_ad) TextView additional_details;
    @BindView(R.id.dd_ot) TextView dose_title;
    @BindView(R.id.dd_od) TextView dose_details;
    @BindView(R.id.dd_bt) TextView brand_title;
    @BindView(R.id.dd_bd) TextView brand_details;

    public ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_drugs);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        pd = new ProgressDialog(DrugDetails.this);
        pd.setCancelable(false);

        Intent drugdets = getIntent();

        drug_id.setText(drugdets.getStringExtra("id"));
        drug_name.setText(drugdets.getStringExtra("drug"));
        getDetails(drugdets.getStringExtra("id"));

        indication_title.setOnClickListener(v->{
            if(indication_details.getVisibility() == GONE){
                indication_details.setVisibility(VISIBLE);
            }else{
                indication_details.setVisibility(GONE);
            }
        });

        moa_title.setOnClickListener(v->{
            if(moa_details.getVisibility() == GONE){
                moa_details.setVisibility(VISIBLE);
            }else{
                moa_details.setVisibility(GONE);
            }
        });

        interaction_title.setOnClickListener(v->{
            if(interaction_details.getVisibility() == GONE){
                interaction_details.setVisibility(VISIBLE);
            }else{
                interaction_details.setVisibility(GONE);
            }
        });

        side_title.setOnClickListener(v->{
            if(side_details.getVisibility() == GONE){
                side_details.setVisibility(VISIBLE);
            }else{
                side_details.setVisibility(GONE);
            }
        });

        pregnancy_title.setOnClickListener(v->{
            if(pregnancy_details.getVisibility() == GONE){
                pregnancy_details.setVisibility(VISIBLE);
            }else{
                pregnancy_details.setVisibility(GONE);
            }
        });

        additional_title.setOnClickListener(v->{
            if(additional_details.getVisibility() == GONE){
                additional_details.setVisibility(VISIBLE);
            }else{
                additional_details.setVisibility(GONE);
            }
        });

        dose_title.setOnClickListener(v->{
            if(dose_details.getVisibility() == GONE){
                dose_details.setVisibility(VISIBLE);
            }else{
                dose_details.setVisibility(GONE);
            }
        });

        brand_title.setOnClickListener(v->{
            if(brand_details.getVisibility() == GONE){
                brand_details.setVisibility(VISIBLE);
            }else{
                brand_details.setVisibility(GONE);
            }
        });

    }

    public void getDetails(final String activeid){
        pd.setMessage("Populating details ...");
        pd.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("drug_id", activeid);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_DRUG_DETAILS, params,
                jObj -> {
                    Log.e("TAG", jObj.toString());
                    pd.hide();
                    JSONObject dObj = null;
                    try {
                        dObj = jObj.getJSONObject("active_details");

                        indication_details.setText(dObj.getString("indications"));
                        moa_details.setText(dObj.getString("mode_of_action"));
                        interaction_details.setText(dObj.getString("AdminDrug_ID"));
                        side_details.setText(dObj.getString("side_effects"));
                        pregnancy_details.setText(dObj.getString("pregnancy_category"));
                        additional_details.setText(dObj.getString("additional_labelling"));
                        dose_details.setText(dObj.getString("dosage"));
                        brand_details.setText(dObj.getString("brand"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Toast.makeText(DrugDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            pd.hide();
        });

        queue.add(jsonObjReq);
    }
}
