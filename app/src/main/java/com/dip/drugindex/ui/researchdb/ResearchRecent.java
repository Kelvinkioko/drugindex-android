package com.dip.drugindex.ui.researchdb;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ResearchAdapter;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.db.viewModel.ClinicalViewModel;
import com.dip.drugindex.api.db.viewModel.ResearchViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.details.ResearchDetails;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class ResearchRecent extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVresearch) RecyclerView RVresearch;
    @BindView(R.id.rr_empty)ImageView empty;

    MaterialSearchView searchView;

    private ResearchAdapter researchAdapter;
    private List<ResearchModel> researchModel;
    private ResearchViewModel researchViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        researchViewModel = ViewModelProviders.of(this).get(ResearchViewModel.class);

        if(researchModel.size() == 0){
            RVresearch.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        researchViewModel.getAllResearch().observe(this, clinicalModels -> {
            if(clinicalModels.size() > 0){
                if (researchModel.size() == 0 ||
                        researchModel.size() > clinicalModels.size() ||
                        researchModel.size() < clinicalModels.size()) {

                    RVresearch.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    researchModel = clinicalModels;
                    researchAdapter.setItems(researchModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.research_recent, null);
        ButterKnife.bind(this, x);

        searchView = getActivity().findViewById(R.id.search_view);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVresearch.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVresearch, (view1, position) -> {
            Intent active = new Intent(getActivity(), ResearchDetails.class);
            TextView ir_category_name = view1.findViewById(R.id.ir_category);
            String str_category = ir_category_name.getText().toString().trim();
            TextView ir_title = view1.findViewById(R.id.ir_title);
            String str_title = ir_title.getText().toString().trim();
            TextView ir_published_by = view1.findViewById(R.id.ir_publisher);
            String str_published_by = ir_published_by.getText().toString().trim();
            TextView ir_url = view1.findViewById(R.id.ir_url);
            String str_url = ir_url.getText().toString().trim();
            TextView ir_image_url = view1.findViewById(R.id.ir_image);
            String str_imageurl = ir_image_url.getText().toString().trim();
            TextView ir_page_count = view1.findViewById(R.id.ir_pagecount);
            String str_pagecount = ir_page_count.getText().toString().trim();
            TextView ir_topics = view1.findViewById(R.id.ir_tags);
            String str_topics = ir_topics.getText().toString().trim();
            TextView ir_price = view1.findViewById(R.id.ir_cost);
            String str_price = ir_price.getText().toString().trim();
            TextView ir_date_published = view1.findViewById(R.id.ir_published);
            String str_date = ir_date_published.getText().toString().trim();


            active.putExtra("category", str_category);
            active.putExtra("title", str_title);
            active.putExtra("publishedby", str_published_by);
            active.putExtra("url", str_url);
            active.putExtra("cimageurl", str_imageurl);
            active.putExtra("pagecount", str_pagecount);
            active.putExtra("topics", str_topics);
            active.putExtra("price", str_price);
            active.putExtra("date", str_date);

            startActivity(active);
        }));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextChange", query);
                if(TextUtils.isEmpty(query)){
                    researchAdapter.getFilter().filter("");
                }else{
                    researchAdapter.getFilter().filter(query.toString());
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextSubmit", newText);
                if(TextUtils.isEmpty(newText)){
                    researchAdapter.getFilter().filter("");
                }else{
                    researchAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        researchModel = new ArrayList<>();

        researchAdapter = new ResearchAdapter(getActivity(), researchModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVresearch.setLayoutManager(mLayoutManager);
        RVresearch.setItemAnimator(new DefaultItemAnimator());
        RVresearch.setAdapter(researchAdapter);

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.m_search);
        searchView.setMenuItem(searchItem);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
