package com.dip.drugindex.ui.setts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dip.drugindex.R;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.views.dialogsheet.DialogSheet;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/25/2018.
 */

public class AddPayment extends AppCompatActivity {

    @BindView(R.id.ssp_paypal) LinearLayout ssp_paypal;
    @BindView(R.id.ssp_mpesa) LinearLayout ssp_mpesa;

    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_select_payment);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Select Payment Methods");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        db = new SQLiteHandler(AddPayment.this);

        ssp_paypal.setOnClickListener(v -> createEmailDialog());
        ssp_mpesa.setOnClickListener(v -> createAndShowDialog());

    }

    private void createAndShowDialog() {
        DialogSheet dialogSheet = new DialogSheet(AddPayment.this);

        dialogSheet.setView(R.layout.dialog_add_mpesa);

        View inflatedView = dialogSheet.getInflatedView();
        EditText number = inflatedView.findViewById(R.id.dam_number);
        TextView cancel = inflatedView.findViewById(R.id.dam_cancel);
        cancel.setOnClickListener(view -> dialogSheet.dismiss());
        TextView ok = inflatedView.findViewById(R.id.dam_ok);
        ok.setOnClickListener(view ->{
            db.addPay("mpesa", number.getText().toString(), "0", "0", "0", "0");
            Toast.makeText(this, "Payment method added successfully!", Toast.LENGTH_SHORT).show();
            dialogSheet.dismiss();
            finish();
        });

        dialogSheet.show();

    }

    private void createEmailDialog() {
        DialogSheet dialogSheet = new DialogSheet(AddPayment.this);

        dialogSheet.setView(R.layout.dialog_add_paypal);

        View inflatedView = dialogSheet.getInflatedView();
        EditText email = inflatedView.findViewById(R.id.dam_email);
        TextView cancel = inflatedView.findViewById(R.id.dam_p_cancel);
        cancel.setOnClickListener(view -> dialogSheet.dismiss());
        TextView ok = inflatedView.findViewById(R.id.dam_p_ok);
        ok.setOnClickListener(view -> {
            db.addPay("paypal", email.getText().toString(), "0", "0", "0", "0");
            Toast.makeText(this, "Payment method added successfully!", Toast.LENGTH_SHORT).show();
            dialogSheet.dismiss();
            finish();
        });

        dialogSheet.show();
    }
}
