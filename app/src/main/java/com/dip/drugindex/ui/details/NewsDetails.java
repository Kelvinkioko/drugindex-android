package com.dip.drugindex.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.core.SQLiteHandler;
import com.google.android.exoplayer2.C;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class NewsDetails extends AppCompatActivity {

    @BindView(R.id.card_panic) CardView card_panic;
    @BindView(R.id.card_normal) CardView card_normal;

    @BindView(R.id.dn_image) ImageView dn_image;

    @BindView(R.id.dn_title) TextView dn_title;
    @BindView(R.id.dn_date) TextView dn_date;
    @BindView(R.id.dn_desc) TextView dn_desc;

    //    Animation
    public Animation slideIn, slideOut;
    private static int DIALOG_TIME_OUT = 2000;

    public String str_id, str_pubdate, str_pubby, str_title, str_desc, str_imageurl;
    private static String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=";
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_news);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("News Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });

        slideIn = AnimationUtils.loadAnimation(this, R.anim.dialog_show);
        slideOut = AnimationUtils.loadAnimation(this, R.anim.dialog_dismiss);

        db = new SQLiteHandler(NewsDetails.this);

        Intent nd = getIntent();
        str_id = nd.getStringExtra("id");
        str_pubdate = nd.getStringExtra("pubdate");
        str_pubby = nd.getStringExtra("pubby");
        str_title = nd.getStringExtra("title");
        str_desc = nd.getStringExtra("desc");
        str_imageurl = nd.getStringExtra("imageurl");

        dn_title.setText(str_title);
        dn_date.setText(str_pubdate + " " + str_pubby);
        dn_desc.setText(str_desc);

        if(str_imageurl.isEmpty() || str_imageurl.equals("null")){
            dn_image.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
        }else{
            Glide.with(this).load(str_imageurl).into(dn_image);
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.news_share:
                String msg = "Drug Index. Download it today ";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, msg + GOOGLE_PLAY_URL + getPackageName());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share..."));
                return true;
            case R.id.news_fav:
                if(db.getCountFav(str_id) == 0) {
                    db.addFav(str_id, str_title, str_desc, str_pubby, str_imageurl, str_pubdate);
                    positiveDisplay();
                }else{
                    db.deleteFav(str_id);
                    negativeDisplay();
                }
                return true;
        }
        return false;
    }

    private void positiveDisplay(){
        card_normal.setVisibility(View.VISIBLE);
        card_normal.startAnimation(slideIn);

        new Handler().postDelayed(() -> {
            card_normal.setVisibility(View.GONE);
            card_normal.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }

    private void negativeDisplay(){
        card_panic.setVisibility(View.VISIBLE);
        card_panic.startAnimation(slideIn);

        new Handler().postDelayed(() -> {
            card_panic.setVisibility(View.GONE);
            card_panic.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }
}
