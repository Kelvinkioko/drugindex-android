package com.dip.drugindex.ui.news;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.NewsCatAdapter;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.api.db.viewModel.NewsCategoryViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class NewsCategory extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVnewscat) RecyclerView RVnewsCategory;
    @BindView(R.id.newscat_empty)ImageView empty;

    private NewsCatAdapter newsCategoryAdapter;
    private List<NewsCategoryModel> newsCategoryModel;
    private NewsCategoryViewModel newsCategoryViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        newsCategoryViewModel = ViewModelProviders.of(this).get(NewsCategoryViewModel.class);

        if(newsCategoryModel.size() == 0){
            RVnewsCategory.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        newsCategoryViewModel.getAllNewsCategory().observe(this, clinicalModels -> {
            if(clinicalModels.size() > 0){
                if (newsCategoryModel.size() == 0 ||
                        newsCategoryModel.size() > clinicalModels.size() ||
                        newsCategoryModel.size() < clinicalModels.size()) {

                    RVnewsCategory.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    newsCategoryModel = clinicalModels;
                    newsCategoryAdapter.setItems(newsCategoryModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.news_category, null);
        ButterKnife.bind(this, x);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVnewsCategory.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVnewsCategory, (view1, position) -> {
            TextView ia_id = view1.findViewById(R.id.inc_id);
            String str_id = ia_id.getText().toString().trim();
            TextView ia_category = view1.findViewById(R.id.inc_category);
            String str_drug = ia_category.getText().toString().trim();

            Intent active = new Intent(getActivity(), NewsByCategory.class);
            active.putExtra("id", str_id);
            active.putExtra("category", str_drug);
            startActivity(active);
        }));

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        newsCategoryModel = new ArrayList<>();

        newsCategoryAdapter = new NewsCatAdapter(getActivity(), newsCategoryModel);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        RVnewsCategory.setLayoutManager(mLayoutManager);
        RVnewsCategory.setItemAnimator(new DefaultItemAnimator());
        RVnewsCategory.setAdapter(newsCategoryAdapter);

    }

}
