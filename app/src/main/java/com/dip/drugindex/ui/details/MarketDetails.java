package com.dip.drugindex.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.navigation.Market;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class MarketDetails extends AppCompatActivity {

    @BindView(R.id.dm_cart) FloatingActionButton dm_cart;
    @BindView(R.id.dm_image) ImageView dm_image;

    @BindView(R.id.dm_title) TextView dm_title;
    @BindView(R.id.dm_edition) TextView dm_edition;
    @BindView(R.id.dm_cost) TextView dm_cost;
    @BindView(R.id.dm_desc) TextView dm_desc;
    @BindView(R.id.dm_id) TextView dm_id;

    public String id, title, edition, price, description, image_url, created_at;

    public SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_market);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        db = new SQLiteHandler(MarketDetails.this);

        Intent mm = getIntent();
        id = mm.getStringExtra("id");
        title = mm.getStringExtra("title");
        edition = mm.getStringExtra("edition");
        price = mm.getStringExtra("cost");
        description = mm.getStringExtra("desc");
        image_url = mm.getStringExtra("imageurl");
        created_at = mm.getStringExtra("createdat");

        dm_title.setText(title);
        dm_edition.setText(edition);
        dm_cost.setText("Ksh. " + price);
        dm_desc.setText(description);
        dm_id.setText(id);

        if(image_url.isEmpty() || image_url.equals("null")){
            dm_image.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
        }else{
            Glide.with(MarketDetails.this).load(image_url).into(dm_image);
        }

        dm_cart.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MarketDetails.this);
            LayoutInflater inflater = MarketDetails.this.getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.dialog_add_cart, null);
            alertDialogBuilder.setView(dialogView);

            SeekBar seek = dialogView.findViewById(R.id.dac_seeker);
            TextView seekval = dialogView.findViewById(R.id.dac_seekerval);
            seekval.setText(String.valueOf(seek.getProgress()));

            TextView quantity = dialogView.findViewById(R.id.dac_quantity);
            quantity.setText("1");
            TextView cost = dialogView.findViewById(R.id.dac_cost);
            cost.setText(" x " + price);
            TextView total = dialogView.findViewById(R.id.dac_total);
            total.setText(price);

            TextView canceldialog = dialogView.findViewById(R.id.dac_cancel);
            TextView acceptdialog = dialogView.findViewById(R.id.dac_ok);

            // create alert dialog
            final AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    seekval.setText(String.valueOf(progress));
                    quantity.setText(String.valueOf(progress));
                    total.setText(String.valueOf(progress * Integer.valueOf(price.replace(",",""))));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            canceldialog.setOnClickListener(v12 -> alertDialog.cancel());

            acceptdialog.setOnClickListener(v1 -> {
                db.addCart(id, title, image_url, seekval.getText().toString().trim(), price);
                alertDialog.dismiss();
            });
        });

    }
}
