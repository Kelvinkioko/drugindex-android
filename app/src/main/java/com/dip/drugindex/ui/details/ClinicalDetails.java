package com.dip.drugindex.ui.details;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ClinicalDetails extends AppCompatActivity {

    @BindView(R.id.clinical_id) TextView clinical_id;
    @BindView(R.id.clinical_drug) TextView clinical_drug;
    
    @BindView(R.id.dc_notestitle) TextView notes_title;
    @BindView(R.id.dc_notesdet) TextView notes_details;
    @BindView(R.id.dc_signtitle) TextView sign_title;
    @BindView(R.id.dc_signdets) TextView sign_details;
    @BindView(R.id.dc_diagnosistitle) TextView diagnosis_title;
    @BindView(R.id.dc_diagnosisdet) TextView diagnosis_details;
    @BindView(R.id.dc_differentialtitle) TextView differential_title;
    @BindView(R.id.dc_differentialdets) TextView differential_details;
    @BindView(R.id.dc_mantitle) TextView man_title;
    @BindView(R.id.dc_mandet) TextView man_details;
    @BindView(R.id.dc_preventtitle) TextView prevent_title;
    @BindView(R.id.dc_preventdets) TextView prevent_details;

    public ProgressDialog pd;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_clinical);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        pd = new ProgressDialog(ClinicalDetails.this);
        pd.setCancelable(false);

        Intent drugdets = getIntent();
        getDetails(drugdets.getStringExtra("id"));

        clinical_id.setText(drugdets.getStringExtra("id"));
        clinical_drug.setText(drugdets.getStringExtra("drug"));

        notes_title.setOnClickListener(v->{
            if(notes_details.getVisibility() == GONE){
                notes_details.setVisibility(VISIBLE);
            }else{
                notes_details.setVisibility(GONE);
            }
        });

        sign_title.setOnClickListener(v->{
            if(sign_details.getVisibility() == GONE){
                sign_details.setVisibility(VISIBLE);
            }else{
                sign_details.setVisibility(GONE);
            }
        });

        diagnosis_title.setOnClickListener(v->{
            if(diagnosis_details.getVisibility() == GONE){
                diagnosis_details.setVisibility(VISIBLE);
            }else{
                diagnosis_details.setVisibility(GONE);
            }
        });

        differential_title.setOnClickListener(v->{
            if(differential_details.getVisibility() == GONE){
                differential_details.setVisibility(VISIBLE);
            }else{
                differential_details.setVisibility(GONE);
            }
        });

        man_title.setOnClickListener(v->{
            if(man_details.getVisibility() == GONE){
                man_details.setVisibility(VISIBLE);
            }else{
                man_details.setVisibility(GONE);
            }
        });

        prevent_title.setOnClickListener(v->{
            if(prevent_details.getVisibility() == GONE){
                prevent_details.setVisibility(VISIBLE);
            }else{
                prevent_details.setVisibility(GONE);
            }
        });

    }

    public void getDetails(final String activeid){
        pd.setMessage("Populating details ...");
        pd.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("active_id", activeid);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_ACTIVE, params,
                jObj -> {
                    Log.e("TAG", jObj.toString());
                    pd.hide();
                    JSONObject dObj = null;
                    try {
                        dObj = jObj.getJSONObject("active_details");

                        notes_details.setText(dObj.getString("indications"));
                        sign_details.setText(dObj.getString("indications"));
                        diagnosis_details.setText(dObj.getString("indications"));
                        differential_details.setText(dObj.getString("indications"));
                        man_details.setText(dObj.getString("indications"));
                        prevent_details.setText(dObj.getString("indications"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Toast.makeText(ClinicalDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            pd.hide();
        });

        queue.add(jsonObjReq);
    }
}
