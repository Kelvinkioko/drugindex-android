package com.dip.drugindex.ui.news;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.NewsAdapter;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.db.viewModel.NewsViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.BookmarkActivity;
import com.dip.drugindex.ui.details.NewsDetails;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class NewsRecent extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVnews) RecyclerView RVnews;
    @BindView(R.id.news_empty)ImageView empty;

    @BindView(R.id.nr_image) ImageView nr_image;

    @BindView(R.id.nr_id) TextView nr_id;
    @BindView(R.id.nr_title) TextView nr_title;
    @BindView(R.id.nr_date) TextView nr_date;
    @BindView(R.id.nr_publisher) TextView nr_publisher;
    @BindView(R.id.nr_catid) TextView nr_catid;
    @BindView(R.id.nr_imageurl) TextView nr_imageurl;
    @BindView(R.id.nr_desc) TextView nr_desc;

    @BindView(R.id.nr_first) RelativeLayout nr_first;

    MaterialSearchView searchView;

    private NewsViewModel newsViewModel;
    private NewsAdapter newsAdapter;
    private List<NewsModel> newsModel;
    private View x;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);

        if(newsModel.size() == 0){
            RVnews.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        newsViewModel.getAllNews().observe(this, clinicalModels -> {
            if(clinicalModels.size() > 0){
                if (newsModel.size() == 0 ||
                        newsModel.size() > clinicalModels.size() ||
                        newsModel.size() < clinicalModels.size()) {

                    RVnews.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    newsModel = clinicalModels;
                    newsAdapter.setItems(newsModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        x = inflater.inflate(R.layout.news_recent, null);
        ButterKnife.bind(this, x);

        searchView = getActivity().findViewById(R.id.search_view);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        nr_first.setOnClickListener(v -> {
            Intent active = new Intent(getActivity(), NewsDetails.class);
            String str_pubdate = nr_date.getText().toString().trim();
            String str_pubby = nr_publisher.getText().toString().trim();
            String str_title = nr_title.getText().toString().trim();
            String str_desc = nr_desc.getText().toString().trim();
            String str_imageurl = nr_imageurl.getText().toString().trim();

            active.putExtra("pubdate", str_pubdate);
            active.putExtra("pubby", str_pubby);
            active.putExtra("title", str_title);
            active.putExtra("desc", str_desc);
            active.putExtra("imageurl", str_imageurl);

            startActivity(active);
            getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });

        RVnews.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVnews, (view1, position) -> {
            Intent active = new Intent(getActivity(), NewsDetails.class);
            TextView in_id = view1.findViewById(R.id.in_id);
            String str_id = in_id.getText().toString().trim();
            TextView in_pubdate = view1.findViewById(R.id.in_pubdate);
            String str_pubdate = in_pubdate.getText().toString().trim();
            TextView in_pubby = view1.findViewById(R.id.in_publishedby);
            String str_pubby = in_pubby.getText().toString().trim();
            TextView in_title= view1.findViewById(R.id.in_title);
            String str_title = in_title.getText().toString().trim();
            TextView in_desc = view1.findViewById(R.id.in_description);
            String str_desc = in_desc.getText().toString().trim();
            TextView in_imageurl = view1.findViewById(R.id.in_imageurl);
            String str_imageurl = in_imageurl.getText().toString().trim();

            active.putExtra("id", str_id);
            active.putExtra("pubdate", str_pubdate);
            active.putExtra("pubby", str_pubby);
            active.putExtra("title", str_title);
            active.putExtra("desc", str_desc);
            active.putExtra("imageurl", str_imageurl);

            startActivity(active);
            getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextChange", query);
                if(TextUtils.isEmpty(query)){
                    newsAdapter.getFilter().filter("");
                }else{
                    newsAdapter.getFilter().filter(query.toString());
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextSubmit", newText);
                if(TextUtils.isEmpty(newText)){
                    newsAdapter.getFilter().filter("");
                }else{
                    newsAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        newsModel = new ArrayList<>();

        newsAdapter = new NewsAdapter(getActivity(), newsModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVnews.setLayoutManager(mLayoutManager);
        RVnews.setItemAnimator(new DefaultItemAnimator());
        RVnews.setAdapter(newsAdapter);

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.m_search);
        searchView.setMenuItem(searchItem);
        MenuItem bkItem = menu.findItem(R.id.action_bookmark);

        if (bkItem != null && bkItem instanceof TextView) {
            ((TextView) bkItem).setTextColor( Color.BLUE ); // Make text colour blue
            ((TextView) bkItem).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16); // Increase font size
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bookmark:
                startActivity(new Intent(getActivity(), BookmarkActivity.class));
                return true;
        }

        return false;
    }

}
