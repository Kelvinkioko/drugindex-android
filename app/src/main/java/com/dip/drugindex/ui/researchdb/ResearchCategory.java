package com.dip.drugindex.ui.researchdb;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ResearchCatAdapter;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;
import com.dip.drugindex.api.db.viewModel.ResearchCategoryViewModel;
import com.dip.drugindex.api.db.viewModel.ResearchViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class ResearchCategory extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVresearchcat) RecyclerView RVresearchcat;
    @BindView(R.id.researchcat_empty)ImageView empty;

    private ResearchCatAdapter researchCategoryAdapter;
    private List<ResearchCategoryModel> researchCategoryModel;
    private ResearchCategoryViewModel researchCategoryViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        researchCategoryViewModel = ViewModelProviders.of(this).get(ResearchCategoryViewModel.class);

        if(researchCategoryModel.size() == 0){
            RVresearchcat.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        researchCategoryViewModel.getAllResearchCategory().observe(this, clinicalModels -> {
            if(clinicalModels.size() > 0){
                if (researchCategoryModel.size() == 0 ||
                        researchCategoryModel.size() > clinicalModels.size() ||
                        researchCategoryModel.size() < clinicalModels.size()) {

                    RVresearchcat.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    researchCategoryModel = clinicalModels;
                    researchCategoryAdapter.setItems(researchCategoryModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.research_category, null);
        ButterKnife.bind(this, x);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVresearchcat.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVresearchcat, (view1, position) -> {
            TextView ia_id = view1.findViewById(R.id.inc_id);
            String str_id = ia_id.getText().toString().trim();
            TextView ia_category = view1.findViewById(R.id.inc_category);
            String str_drug = ia_category.getText().toString().trim();

            Intent active = new Intent(getActivity(), ResearchByCategory.class);
            active.putExtra("id", str_id);
            active.putExtra("category", str_drug);
            startActivity(active);
        }));

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        researchCategoryModel = new ArrayList<>();

        researchCategoryAdapter = new ResearchCatAdapter(getActivity(), researchCategoryModel);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        RVresearchcat.setLayoutManager(mLayoutManager);
        RVresearchcat.setItemAnimator(new DefaultItemAnimator());
        RVresearchcat.setAdapter(researchCategoryAdapter);

    }

}
