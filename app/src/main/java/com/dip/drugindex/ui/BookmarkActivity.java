package com.dip.drugindex.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.FavAdapter;
import com.dip.drugindex.api.db.models.FavouriteModel;
import com.dip.drugindex.core.SQLiteHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 5/10/2018.
 */

public class BookmarkActivity extends AppCompatActivity {

    @BindView(R.id.card_panic) CardView card_panic;
    @BindView(R.id.card_normal) CardView card_normal;

    @BindView(R.id.RVbookmarks) RecyclerView RVpayment;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipeContainer;
    @BindView(R.id.bookmarks_empty) ImageView empty;

    //    Animation
    public Animation slideIn, slideOut;
    //Timer Delay
    private static int DIALOG_TIME_OUT = 2000;

    private SQLiteHandler db;
    private FavAdapter fAdapter;
    private List<FavouriteModel> fModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Bookmarks");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });

        slideIn = AnimationUtils.loadAnimation(this, R.anim.dialog_show);
        slideOut = AnimationUtils.loadAnimation(this, R.anim.dialog_dismiss);

        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        db = new SQLiteHandler(BookmarkActivity.this);

        swipeContainer.setOnRefreshListener(() -> {
           fetch();
        });

        fModel = new ArrayList<>();
        fAdapter = new FavAdapter(BookmarkActivity.this, fModel);

        fetch();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BookmarkActivity.this, LinearLayoutManager.HORIZONTAL, false);
        RVpayment.setLayoutManager(mLayoutManager);
        RVpayment.setItemAnimator(new DefaultItemAnimator());
        RVpayment.setAdapter(fAdapter);

    }

    public void fetch(){
        swipeContainer.setRefreshing(false);
        if(db.countFav()) {
            Cursor cursor = db.getFav();
            if (cursor.getCount() > 0) {
//                positiveDisplay();
                RVpayment.setVisibility(View.VISIBLE);
                empty.setVisibility(View.GONE);
                if (cursor.moveToFirst()) {
                    do {
                        FavouriteModel item = new FavouriteModel();
                        item.setId(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_ID)));
                        item.setTitle(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_TITLE)));
                        item.setDesc(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_DESC)));
                        item.setPostby(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_PUBBY)));
                        item.setImageurl(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_IMAGEURL)));
                        item.setPostdate(cursor.getString(cursor.getColumnIndex(SQLiteHandler.FAV_PUBDATE)));

                        fModel.add(item);

                    } while (cursor.moveToNext());
                    fAdapter.notifyDataSetChanged();
                }
            } else {
                negativeDisplay();
                RVpayment.setVisibility(View.GONE);
                empty.setVisibility(View.VISIBLE);
            }
        } else {
            negativeDisplay();
            RVpayment.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        }
    }

    private void positiveDisplay(){
        card_normal.setVisibility(View.VISIBLE);
        card_normal.startAnimation(slideIn);

        new Handler().postDelayed(() -> {
            card_normal.setVisibility(View.GONE);
            card_normal.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }

    private void negativeDisplay(){
        card_panic.setVisibility(View.VISIBLE);
        card_panic.startAnimation(slideIn);

        new Handler().postDelayed(() -> {
            card_panic.setVisibility(View.GONE);
            card_panic.startAnimation(slideOut);
        }, DIALOG_TIME_OUT);
    }
}
