package com.dip.drugindex.ui.drugindex;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ActiveAdapter;
import com.dip.drugindex.api.db.adapter.IndexAdapter;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.db.models.IndexModel;
import com.dip.drugindex.api.db.viewModel.ActiveViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.ui.details.DrugDetails;
import com.dip.drugindex.views.RotateLoading;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class DIActive extends Fragment {

    @BindView(R.id.da_progress_lay) RelativeLayout hap_lay;
    @BindView(R.id.da_lay) RelativeLayout da_lay;
    @BindView(R.id.da_progress) RotateLoading da_progress;

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVactive) RecyclerView RVactive;
    @BindView(R.id.da_empty)ImageView empty;

    MaterialSearchView searchView;

    private SQLiteHandler db;
    private int search = 0;

    private ActiveAdapter activeAdapter;
    private List<ActiveModel> activeModel;
    private ActiveViewModel activeViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activeViewModel = ViewModelProviders.of(this).get(ActiveViewModel.class);

        if(activeModel.size() == 0){
            RVactive.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        activeViewModel.getAllActive().observe(this, activeModels -> {
            if(activeModels.size() > 0){
                if (activeModel.size() == 0 ||
                        activeModel.size() > activeModels.size() ||
                        activeModel.size() < activeModels.size()) {

                    RVactive.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    activeModel = activeModels;
                    activeAdapter.setItems(activeModel);
                }
            }
        });
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.drug_active, null);
        ButterKnife.bind(this, x);

        searchView = getActivity().findViewById(R.id.search_view);

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVactive.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVactive, (view1, position) -> {

            TextView ia_id = view1.findViewById(R.id.ii_id);
            String str_id = ia_id.getText().toString().trim();
            TextView ia_drug = view1.findViewById(R.id.ii_title);
            String str_drug = ia_drug.getText().toString().trim();

            if(search == 1){
                db.addSearch(str_id, str_drug, "");
                search = 0;
            }

            Intent active = new Intent(getActivity(), DrugDetails.class);
            active.putExtra("id", str_id);
            active.putExtra("drug", str_drug);
            startActivity(active);
        }));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = 1;
                Log.i("onQueryTextChange", query);
                if(TextUtils.isEmpty(query)){
                    activeAdapter.getFilter().filter("");
                }else{
                    activeAdapter.getFilter().filter(query.toString());
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search = 1;
                Log.i("onQueryTextSubmit", newText);
                if(TextUtils.isEmpty(newText)){
                    activeAdapter.getFilter().filter("");
                }else{
                    activeAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activeModel = new ArrayList<>();

        activeAdapter = new ActiveAdapter(getActivity(), activeModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVactive.setLayoutManager(mLayoutManager);
        RVactive.setItemAnimator(new DefaultItemAnimator());
        RVactive.setAdapter(activeAdapter);

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView.setMenuItem(searchItem);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
