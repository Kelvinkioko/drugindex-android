package com.dip.drugindex.ui.help;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.ui.help.faq.FaqActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class HelpFaq extends AppCompatActivity {

    @BindView(R.id.hf_sign) TextView hf_sign;
    @BindView(R.id.hf_data) TextView hf_data;
    @BindView(R.id.hf_login) TextView hf_login;

    @BindView(R.id.hf_work) TextView hf_work;
    @BindView(R.id.hf_ads) TextView hf_ads;
    @BindView(R.id.hf_listing) TextView hf_listing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_faq);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("FAQs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        hf_sign.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "Sign up");
            sgn.putExtra("desc", hf_sign.getText().toString().trim());
            startActivity(sgn);
        });
        hf_data.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "Data Security");
            sgn.putExtra("desc", hf_data.getText().toString().trim());
            startActivity(sgn);
        });
        hf_login.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "Device Login");
            sgn.putExtra("desc", hf_login.getText().toString().trim());
            startActivity(sgn);
        });

        hf_work.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "How to");
            sgn.putExtra("desc", hf_work.getText().toString().trim());
            startActivity(sgn);
        });
        hf_ads.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "Ads");
            sgn.putExtra("desc", hf_ads.getText().toString().trim());
            startActivity(sgn);
        });
        hf_listing.setOnClickListener(v->{
            Intent sgn = new Intent(HelpFaq.this, FaqActivity.class);
            sgn.putExtra("title", "Incorrect Listing");
            sgn.putExtra("desc", hf_listing.getText().toString().trim());
            startActivity(sgn);
        });

    }
}
