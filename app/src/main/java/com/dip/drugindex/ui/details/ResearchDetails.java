package com.dip.drugindex.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.ui.researchdb.ResearchReview;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchDetails extends AppCompatActivity {

    @BindView(R.id.dr_preview) FloatingActionButton dr_preview;

    @BindView(R.id.dr_image) ImageView dr_image;

    @BindView(R.id.dr_title) TextView dr_title;
    @BindView(R.id.dr_publishdate) TextView dr_publishdate;

    @BindView(R.id.dr_pubby) TextView dr_pubby;
    @BindView(R.id.dr_url) TextView dr_url;
    @BindView(R.id.dr_pages) TextView dr_pages;
    @BindView(R.id.dr_cart) TextView dr_cart;

    public String str_title, str_publishdate, str_pubby, str_url, str_pages, str_image, str_price, str_topics;

    public SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_research);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        db = new SQLiteHandler(ResearchDetails.this);

        dr_preview.setOnClickListener(v -> startActivity(new Intent(ResearchDetails.this, ResearchReview.class)));

        Intent active = getIntent();
        str_title = active.getStringExtra("title");
        str_pubby = active.getStringExtra("publishedby");
        str_url = active.getStringExtra("url");
        str_image = active.getStringExtra("cimageurl");
        str_pages = active.getStringExtra("pagecount");
        str_topics = active.getStringExtra("topics");
        str_price = active.getStringExtra("price");
        str_publishdate = active.getStringExtra("date");

        dr_title.setText(str_title);
        dr_publishdate.setText(str_publishdate);
        dr_pubby.setText(str_pubby);
        dr_url.setText(str_url);
        dr_pages.setText(str_pages);

        if(str_image.isEmpty() || str_image.equals("null")){
            dr_image.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
        }else{
            Glide.with(this).load(str_image).into(dr_image);
        }

        dr_cart.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ResearchDetails.this);
            LayoutInflater inflater = ResearchDetails.this.getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.dialog_research_cart, null);
            alertDialogBuilder.setView(dialogView);

            TextView title = dialogView.findViewById(R.id.drc_title);
            title.setText(str_title);
            TextView cost = dialogView.findViewById(R.id.drc_total);
            cost.setText(str_price);

            TextView canceldialog = dialogView.findViewById(R.id.drc_cancel);
            TextView acceptdialog = dialogView.findViewById(R.id.drc_ok);

            // create alert dialog
            final AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            canceldialog.setOnClickListener(v12 -> alertDialog.cancel());

            acceptdialog.setOnClickListener(v1 -> {
                db.addCart("1", str_title, str_image, "1", str_price);
                alertDialog.dismiss();
            });
        });

    }
}
