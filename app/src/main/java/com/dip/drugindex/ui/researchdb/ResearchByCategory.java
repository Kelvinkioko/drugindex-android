package com.dip.drugindex.ui.researchdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ResearchAdapter;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.details.ResearchDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class ResearchByCategory extends AppCompatActivity {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVrbc) RecyclerView RVrbc;
    @BindView(R.id.rbc_empty)ImageView empty;

    private ResearchAdapter dmAdapter;
    private List<ResearchModel> dmModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_by_category);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        Intent dets = getIntent();
        setTitle(dets.getStringExtra("category"));
        fetchAI(dets.getStringExtra("id"));

        dmModel = new ArrayList<ResearchModel>();

        dmAdapter = new ResearchAdapter(ResearchByCategory.this, dmModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ResearchByCategory.this);
        RVrbc.setLayoutManager(mLayoutManager);
        RVrbc.setItemAnimator(new DefaultItemAnimator());
        RVrbc.setAdapter(dmAdapter);

        RVrbc.addOnItemTouchListener(new RecyclerTouchListener(ResearchByCategory.this, RVrbc, (view1, position) -> {
            Intent active = new Intent(ResearchByCategory.this, ResearchDetails.class);
            startActivity(active);
        }));

    }

    public void fetchAI(final String category_id){
        swipecontainer.setRefreshing(true);
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("category_id", category_id);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_RESEARCH_BY_CATEGORY, params,
                jObj -> {
                    Log.e("TAG", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("research");

                        Log.e("Research", feedArray.toString());
                        if(feedArray.length() > 0){
                            dmModel.clear();
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                ResearchModel item = new ResearchModel();;
                                item.setCategory_name(feedObj.getString("category_name"));
                                item.setTitle(feedObj.getString("title"));
                                item.setPublished_by(feedObj.getString("published_by"));
                                item.setUrl(feedObj.getString("url"));
                                item.setImage_url(feedObj.getString("image_url"));
                                item.setPage_count(feedObj.getString("page_count"));
                                item.setTopics(feedObj.getString("topics"));
                                item.setPrice(feedObj.getString("price"));
                                item.setDate_published(feedObj.getString("date_published"));

                                dmModel.add(item);
                            }
                            dmAdapter.notifyDataSetChanged();
                        }else{
                            if(dmModel.size() > 0) {
                                RVrbc.setVisibility(VISIBLE);
                                empty.setVisibility(GONE);
                            } else {
                                RVrbc.setVisibility(GONE);
                                empty.setVisibility(VISIBLE);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            swipecontainer.setRefreshing(false);
            if(dmModel.size() > 0) {
                RVrbc.setVisibility(VISIBLE);
                empty.setVisibility(GONE);
            } else {
                RVrbc.setVisibility(GONE);
                empty.setVisibility(VISIBLE);
            }
        });

        queue.add(jsonObjReq);
    }
}
