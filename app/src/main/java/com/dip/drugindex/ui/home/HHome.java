package com.dip.drugindex.ui.home;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.AdAdapter;
import com.dip.drugindex.api.db.adapter.SearchAdapter;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.db.models.SearchModel;
import com.dip.drugindex.api.db.viewModel.AdvertisementViewModel;
import com.dip.drugindex.api.db.viewModel.SearchViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.views.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class HHome extends Fragment {

    @BindView(R.id.ad_lay) RelativeLayout ad_lay;
    @BindView(R.id.hh_lay) RelativeLayout hh_lay;
    @BindView(R.id.hh_progress) RotateLoading hh_progress;

    @BindView(R.id.RVsearch) RecyclerView RVsearch;
    @BindView(R.id.hh_empty)ImageView empty;

    @BindView(R.id.RVad) RecyclerView RVad;
    @BindView(R.id.ad_empty)ImageView ad_empty;

    private AdAdapter advertisementAdapter;
    private List<AdvertisementModel> advertisementModel;
    private AdvertisementViewModel advertisementViewModel;

    private SQLiteHandler db;

    private SearchAdapter searchAdapter;
    private List<SearchModel> searchModel;
    private SearchViewModel searchViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        advertisementViewModel = ViewModelProviders.of(this).get(AdvertisementViewModel.class);
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);

        if(searchModel.size() == 0){
            RVsearch.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        advertisementViewModel.getAllAdvertisement().observe(this, advertisementModels -> {
            if(advertisementModels.size() > 0){
                if (advertisementModel.size() == 0 ||
                        advertisementModel.size() > advertisementModels.size() ||
                        advertisementModel.size() < advertisementModels.size()) {

                    RVsearch.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    advertisementModel = advertisementModels;
                    advertisementAdapter.setServices(advertisementModel);
                }
            }
        });

        searchViewModel.getAllSearch().observe(this, servicesModels -> {
            if(servicesModels.size() > 0){
                if (searchModel.size() == 0 ||
                        searchModel.size() > servicesModels.size() ||
                        searchModel.size() < servicesModels.size()) {

                    RVsearch.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    searchModel = servicesModels;
                    searchAdapter.setSearch(searchModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View x = inflater.inflate(R.layout.home_home, null);
        ButterKnife.bind(this, x);
        db = new SQLiteHandler(getActivity());

        RVad.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVad, (view1, position) -> {

            TextView ia_desc = view1.findViewById(R.id.ia_description);
            String str_desc = ia_desc.getText().toString().trim();

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str_desc)));
        }));

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        advertisementModel = new ArrayList<>();

        advertisementAdapter = new AdAdapter(getActivity(), advertisementModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        RVad.setLayoutManager(mLayoutManager);
        RVad.setItemAnimator(new DefaultItemAnimator());
        RVad.setAdapter(advertisementAdapter);

        searchModel = new ArrayList<>();

        searchAdapter = new SearchAdapter(getActivity(), searchModel);
        RecyclerView.LayoutManager sLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RVsearch.setLayoutManager(sLayoutManager);
        RVsearch.setItemAnimator(new DefaultItemAnimator());
        RVsearch.setAdapter(searchAdapter);

    }

    public void fetchSearch(){
        Cursor cursor = db.getSearch();

        if(cursor.getCount() > 0) {
            RVsearch.setVisibility(VISIBLE);
            empty.setVisibility(GONE);
            if (cursor.moveToFirst()) {
                searchModel.clear();
                do {
                    SearchModel item = new SearchModel();
                    item.setId(cursor.getString(cursor.getColumnIndex(SQLiteHandler.SCH_ID)));
                    item.setDrug(cursor.getString(cursor.getColumnIndex(SQLiteHandler.SCH_TITLE)));
                    item.setActiveing(cursor.getString(cursor.getColumnIndex(SQLiteHandler.SCH_DESC)));

                    searchModel.add(item);

                } while (cursor.moveToNext());
                searchAdapter.notifyDataSetChanged();
            }
        }else{
            RVsearch.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

}
