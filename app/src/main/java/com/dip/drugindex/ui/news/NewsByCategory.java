package com.dip.drugindex.ui.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.NewsAdapter;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.details.NewsDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsByCategory extends AppCompatActivity {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVnbc) RecyclerView RVnbc;
    @BindView(R.id.nbc_empty)ImageView empty;

    private NewsAdapter dmAdapter;
    private List<NewsModel> dmModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_by_category);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        Intent dets = getIntent();
        setTitle(dets.getStringExtra("category"));
        fetchAI(dets.getStringExtra("id"));

        dmModel = new ArrayList<NewsModel>();

        dmAdapter = new NewsAdapter(NewsByCategory.this, dmModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NewsByCategory.this);
        RVnbc.setLayoutManager(mLayoutManager);
        RVnbc.setItemAnimator(new DefaultItemAnimator());
        RVnbc.setAdapter(dmAdapter);

        RVnbc.addOnItemTouchListener(new RecyclerTouchListener(NewsByCategory.this, RVnbc, (view1, position) -> {
            Intent active = new Intent(NewsByCategory.this, NewsDetails.class);
            startActivity(active);
        }));

    }

    public void fetchAI(final String category_id){
        swipecontainer.setRefreshing(true);
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        try{
            params.put("category_id", category_id);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlConfig.URL_NEWS_BY_CATEGORY, params,
                jObj -> {
                    Log.e("TAG", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("news");

                        if(feedArray.length() > 0){
                            dmModel.clear();
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                NewsModel item = new NewsModel();
                                item.setId(feedObj.getInt("id"));
                                item.setCategory_id(feedObj.getString("category_id"));
                                item.setPublished_date(feedObj.getString("published_date"));
                                item.setPublished_by(feedObj.getString("published_by"));
                                item.setTitle(feedObj.getString("title"));
                                item.setDescription(Html.fromHtml(feedObj.getString("description")).toString());
                                item.setImage_url(feedObj.getString("image_url"));

                                dmModel.add(item);
                            }
                            dmAdapter.notifyDataSetChanged();
                        }else{
                            if(dmModel.size() > 0) {
                                RVnbc.setVisibility(VISIBLE);
                                empty.setVisibility(GONE);
                            } else {
                                RVnbc.setVisibility(GONE);
                                empty.setVisibility(VISIBLE);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            swipecontainer.setRefreshing(false);
            if(dmModel.size() > 0) {
                RVnbc.setVisibility(VISIBLE);
                empty.setVisibility(GONE);
            } else {
                RVnbc.setVisibility(GONE);
                empty.setVisibility(VISIBLE);
            }
        });

        queue.add(jsonObjReq);
    }
}
