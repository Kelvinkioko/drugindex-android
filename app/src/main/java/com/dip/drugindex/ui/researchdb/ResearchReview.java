package com.dip.drugindex.ui.researchdb;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.dip.drugindex.R;
import com.dip.drugindex.views.pdfView.RemotePDFViewPager;
import com.dip.drugindex.views.pdfView.adapter.PDFPagerAdapter;
import com.dip.drugindex.views.pdfView.remote.DownloadFile;
import com.dip.drugindex.views.pdfView.util.FileUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/27/2018.
 */

public class ResearchReview extends AppCompatActivity implements DownloadFile.Listener {

    @BindView(R.id.ed_bar) ProgressBar ed_bar;
    @BindView(R.id.root) LinearLayout root;

    public RemotePDFViewPager _pdf;
    public PDFPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_review);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Preview");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        Context ctx = this;
        DownloadFile.Listener listener = this;
        _pdf = new RemotePDFViewPager(ctx, "www.epzakenya.com/UserFiles/files/Pharmaceutical.pdf", listener);
        _pdf.setId(R.id.pdfViewPager);

    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        Log.e("details", "Pdf was loaded successfully");
        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
        _pdf.setAdapter(adapter);
        updateLayout();
    }

    @Override
    public void onFailure(Exception e) {

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        Log.e("details", String.valueOf(progress));
        ed_bar.setMax(total);
        ed_bar.setProgress(progress);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.close();
        }
    }

    public void updateLayout() {
        Log.e("details", "Layout updated successfully");
        root.removeAllViewsInLayout();
        root.addView(_pdf, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }
}
