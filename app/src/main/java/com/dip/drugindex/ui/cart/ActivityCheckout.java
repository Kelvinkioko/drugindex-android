package com.dip.drugindex.ui.cart;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.dip.drugindex.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

public class ActivityCheckout extends AppCompatActivity {

    @BindView(R.id.as_fname) EditText fullname;
    @BindView(R.id.as_location) EditText location;
    @BindView(R.id.as_number) EditText number;
    @BindView(R.id.as_county) EditText county;

    @BindView(R.id.as_termsc) TextView termsc;
    @BindView(R.id.as_pay) TextView pay;

    @BindView(R.id.as_check) CheckBox check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Check-out");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        if(check.isChecked()){
            pay.setEnabled(true);
        }else{
            pay.setEnabled(false);
        }

        termsc.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityCheckout.this);
            // Get the layout inflater
            LayoutInflater inflater = ActivityCheckout.this.getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            View dialogView = inflater.inflate(R.layout.dialog_terms, null);
            alertDialogBuilder.setView(dialogView);


            TextView dismiss = dialogView.findViewById(R.id.dt_dismiss);

            // create alert dialog
            final AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            dismiss.setOnClickListener(v1 -> alertDialog.dismiss());
        });

    }
}
