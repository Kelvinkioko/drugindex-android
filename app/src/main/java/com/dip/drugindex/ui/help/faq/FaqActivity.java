package com.dip.drugindex.ui.help.faq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.dip.drugindex.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 4/21/2018.
 */

public class FaqActivity extends AppCompatActivity {

    @BindView(R.id.af_title) TextView af_title;
    @BindView(R.id.af_desc) TextView af_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        Intent sgn = getIntent();
        setTitle(sgn.getStringExtra("title"));
        af_title.setText(sgn.getStringExtra("desc"));
        if(sgn.getStringExtra("title").equals("Sign up")){
            af_desc.setText(getResources().getString(R.string.faq_details_sign));
        }else if(sgn.getStringExtra("title").equals("Data Security")){
            af_desc.setText(getResources().getString(R.string.faq_details_data));
        }else if(sgn.getStringExtra("title").equals("Device Login")){
            af_desc.setText(getResources().getString(R.string.faq_details_login));
        }else if(sgn.getStringExtra("title").equals("How to")){
            af_desc.setText(getResources().getString(R.string.faq_details_work));
        }else if(sgn.getStringExtra("title").equals("Ads")){
            af_desc.setText(getResources().getString(R.string.faq_details_ads));
        }else{
            af_desc.setText(getResources().getString(R.string.faq_details_products));
        }

    }
}
