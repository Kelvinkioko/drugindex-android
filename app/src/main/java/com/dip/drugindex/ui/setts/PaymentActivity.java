package com.dip.drugindex.ui.setts;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.PaymentAdapter;
import com.dip.drugindex.api.db.models.PaymentModel;
import com.dip.drugindex.core.SQLiteHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/25/2018.
 */

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.sp_payment_add) TextView sp_payment_add;

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVpayment) RecyclerView RVpayment;
    @BindView(R.id.sp_empty)ImageView empty;

    private PaymentAdapter pmAdapter;
    private List<PaymentModel> pmModel;

    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_payment);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Payment Methods");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        db = new SQLiteHandler(PaymentActivity.this);

        sp_payment_add.setOnClickListener(v->startActivity(new Intent(PaymentActivity.this, AddPayment.class)));

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        pmModel = new ArrayList<PaymentModel>();

        pmAdapter = new PaymentAdapter(PaymentActivity.this, pmModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PaymentActivity.this);
        RVpayment.setLayoutManager(mLayoutManager);
        RVpayment.setItemAnimator(new DefaultItemAnimator());
        RVpayment.setAdapter(pmAdapter);

        fetchPayment();

    }

    public void fetchPayment(){
        swipecontainer.setRefreshing(true);
        Cursor cursor = db.getPay();
        if (cursor.moveToFirst()) {
            pmModel.clear();
            do {
                PaymentModel item = new PaymentModel();
                item.setType(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_TYPE)));
                item.setKey(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_KEY)));
                item.setName(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_NAME)));
                item.setCard(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_CARD)));
                item.setExpire(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_EXPIRE)));
                item.setCvv(cursor.getString(cursor.getColumnIndex(SQLiteHandler.PAY_CVV)));

                pmModel.add(item);

            } while (cursor.moveToNext());
            pmAdapter.notifyDataSetChanged();
            swipecontainer.setRefreshing(false);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        fetchPayment();
    }

}
