package com.dip.drugindex.ui.cart;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.CartAdapter;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.ui.setts.AddPayment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

public class ActivityCart extends AppCompatActivity implements CartAdapter.OnItemClickListener {

    @BindView(R.id.ac_total) TextView ac_total;
    @BindView(R.id.ac_checkout) TextView ac_checkout;

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVcart) RecyclerView RVcart;
    @BindView(R.id.ac_empty)ImageView empty;

    private CartAdapter cmAdapter;
    private List<CartModel> cmModel;

    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.dialog_show, R.anim.dialog_dismiss);
        });

        db = new SQLiteHandler(ActivityCart.this);

        ac_total.setText(String.valueOf(db.getPayable()));

        ac_checkout.setOnClickListener(v->startActivity(new Intent(ActivityCart.this, AddPayment.class)));

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        cmModel = new ArrayList<CartModel>();

        cmAdapter = new CartAdapter(ActivityCart.this, cmModel);
        cmAdapter.setOnFeedItemClickListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityCart.this);
        RVcart.setLayoutManager(mLayoutManager);
        RVcart.setItemAnimator(new DefaultItemAnimator());
        RVcart.setAdapter(cmAdapter);

        fetchCart();

    }

    public void fetchCart(){
        swipecontainer.setRefreshing(true);
        Cursor cursor = db.getCart();
        if (cursor.moveToFirst()) {
            cmModel.clear();
            do {
                CartModel item = new CartModel();
                item.setId(cursor.getInt(cursor.getColumnIndex(SQLiteHandler.CART_ID)));
                item.setName(cursor.getString(cursor.getColumnIndex(SQLiteHandler.CART_NAME)));
                item.setImage(cursor.getString(cursor.getColumnIndex(SQLiteHandler.CART_IMAGE)));
                item.setCost(cursor.getString(cursor.getColumnIndex(SQLiteHandler.CART_COST)));
                item.setQuantity(cursor.getString(cursor.getColumnIndex(SQLiteHandler.CART_QUANTITY)));

                cmModel.add(item);

            } while (cursor.moveToNext());
            cmAdapter.notifyDataSetChanged();
            swipecontainer.setRefreshing(false);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        fetchCart();
    }

    @Override
    public void detailsClick(View view, int position) {

    }

    @Override
    public void optionsClick(View view, int position) {

    }
}
