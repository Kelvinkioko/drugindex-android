package com.dip.drugindex.ui.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.authentication.SigninActivity;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.core.SessionManager;
import com.dip.drugindex.utils.ImageHelper;
import com.dip.drugindex.utils.ImagePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 3/13/2018.
 */

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.ap_profile) ImageView ap_profile;
    @BindView(R.id.ap_name) TextView ap_name;
    @BindView(R.id.ap_email) TextView ap_email;
    @BindView(R.id.ap_subscription) TextView ap_subscription;
    @BindView(R.id.ap_update) TextView ap_update;
    @BindView(R.id.ap_logout) TextView ap_logout;

    private SQLiteHandler db;
    private SessionManager session;

    HashMap<String, String> user;

    private String str_pic, str_userid;
    private static final int IMAGE = 101;
    private boolean isImage = false;
    private ProgressDialog pd;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        db = new SQLiteHandler(this);
        session = new SessionManager(this);
        user = db.getUserDetails();

        str_userid = user.get("userid");
        ap_name.setText(user.get("username"));
        ap_email.setText(user.get("useremail"));

        str_pic = user.get("profilepicture");

        if(!str_pic.isEmpty() && !str_pic.equals("null")) {
            Glide.with(ProfileActivity.this).load(user.get("profilepicture"))
                    .into(ap_profile);
        }else{
            ap_profile.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
        }

        ap_profile.setOnClickListener(v->{startActivityForResult(ImagePicker.getPickImageIntent(this), IMAGE);});

        ap_logout.setOnClickListener(v->{
            finishAffinity();
            startActivity(new Intent(this, SigninActivity.class));
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent paramIntent) {
        if (requestCode == IMAGE) {
            bitmap = ImagePicker.getImageFromResult(this, resultCode, paramIntent);
            if (bitmap != null) {
                ap_profile.setImageBitmap(bitmap);
                ap_profile.setScaleType(ImageView.ScaleType.CENTER_CROP);
                isImage = true;
            }
        }
    }

    @Override
    public void onBackPressed(){
        if (isImage){
            pd.setMessage("Updating picture ...");
            pd.show();

            StringRequest objRequest = new StringRequest(Request.Method.POST, UrlConfig.URL_UPDATE_PIC,
                    response -> {
                        pd.hide();

                        try {
                            JSONObject jObj = new JSONObject(response);
                            Log.e("TAG", jObj.toString());
                            String error = jObj.getString("result");
                            if (error.equals("success")) {
                                JSONObject fObj = jObj.getJSONObject("user");

                                db.updateUser(str_userid, fObj.getString("name"), fObj.getString("email"),
                                        fObj.getString("phone_number"), fObj.getString("image_url"));
                                super.onBackPressed();
                                Toast.makeText(this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                String errorMsg = jObj.getString("message");
                                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        pd.hide();
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("prof_img", ImageHelper.getStringFromBitmap(bitmap));
                    params.put("user_id", str_userid);
                    return params;
                }
            };

            Volley.newRequestQueue(this).add(objRequest);
        }else{
            super.onBackPressed();
        }
    }

    public void updatePicture(final String str_image) {

    }
}
