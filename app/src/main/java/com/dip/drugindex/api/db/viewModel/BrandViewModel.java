package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.BrandDao;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */


public class BrandViewModel extends AndroidViewModel {

    LiveData<List<BrandModel>> liveBrand;

    public BrandViewModel (Application application) {
        super(application);
    }

    public LiveData<List<BrandModel>> getAllBrand(){
        try {
            liveBrand = new fetchBrandTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveBrand;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchBrandTask extends AsyncTask<Void, Void, LiveData<List<BrandModel>>> {
        @Override
        protected LiveData<List<BrandModel>> doInBackground(Void... voids){
            MutableLiveData<List<BrandModel>> tempModel = new MutableLiveData<>();
            List<BrandModel> tempBrandModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_BRANDS, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONArray feedArray = new JSONArray(jsonString);
                    if(feedArray.length() > 0){
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            BrandModel item = new BrandModel();
                            item.setBrandid(feedObj.getString("brand_id"));
                            item.setBrand(feedObj.getString("brand"));
                            item.setManufacturer(feedObj.getString("manufacturers"));

                            tempBrandModel.add(item);
                            tempModel.postValue(tempBrandModel);
                        }
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }

}