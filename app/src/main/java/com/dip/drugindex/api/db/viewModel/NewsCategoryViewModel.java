package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.NewsCategoryDao;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsCategoryViewModel extends AndroidViewModel {

    LiveData<List<NewsCategoryModel>> liveNewsCategory;

    public NewsCategoryViewModel (Application application) {
        super(application);
    }

    public LiveData<List<NewsCategoryModel>> getAllNewsCategory(){
        try {
            liveNewsCategory = new fetchNewsCategoryTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveNewsCategory;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchNewsCategoryTask extends AsyncTask<Void, Void, LiveData<List<NewsCategoryModel>>> {
        @Override
        protected LiveData<List<NewsCategoryModel>> doInBackground(Void... voids){
            MutableLiveData<List<NewsCategoryModel>> tempModel = new MutableLiveData<>();
            List<NewsCategoryModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_NEWS_CATEGORY, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    JSONArray feedArray = jObj.getJSONArray("news_category");

                    if(feedArray.length() > 0){
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            NewsCategoryModel item = new NewsCategoryModel();;
                            item.setId(feedObj.getInt("id"));
                            item.setCategory_name(feedObj.getString("category_name"));
                            item.setImage_url(feedObj.getString("image_url"));

                            tempServiceModel.add(item);
                            tempModel.postValue(tempServiceModel);
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }

}
