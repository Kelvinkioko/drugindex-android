package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

@Entity(tableName = "news")
public class NewsModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String category_id, title, description, published_date, published_by, image_url, is_published, created_at, updated_at;

    public NewsModel() { }

    public NewsModel(@NonNull int id, String category_id, String title, String description, String published_date,
                     String published_by, String image_url, String is_published, String created_at, String updated_at) {
        this.id = id;
        this.category_id = category_id;
        this.title = title;
        this.description = description;
        this.published_date = published_date;
        this.published_by = published_by;
        this.image_url = image_url;
        this.is_published = is_published;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublished_date() {
        return published_date;
    }

    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }

    public String getPublished_by() {
        return published_by;
    }

    public void setPublished_by(String published_by) {
        this.published_by = published_by;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getIs_published() {
        return is_published;
    }

    public void setIs_published(String is_published) {
        this.is_published = is_published;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
