package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Entity(tableName = "active")
public class ActiveModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String drugid, drug, active;

    public ActiveModel() {}

    public ActiveModel(@NonNull int id, String drugid, String drug, String active) {
        this.id = id;
        this.drugid = drugid;
        this.drug = drug;
        this.active = active;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getDrugid() {
        return drugid;
    }

    public void setDrugid(String drugid) {
        this.drugid = drugid;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}