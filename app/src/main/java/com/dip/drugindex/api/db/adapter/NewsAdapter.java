package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.core.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> implements Filterable {

    private SessionManager sess;
    private List<NewsModel> dmItems;
    private List<NewsModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<NewsModel> results = new ArrayList<NewsModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final NewsModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<NewsModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView in_title, in_id, in_categoryid, in_pubdate, in_pubby, in_desc, in_imageurl;
        public ImageView in_image;

        public MyViewHolder(View view) {
            super(view);
            in_id = view.findViewById(R.id.in_id);
            in_categoryid = view.findViewById(R.id.in_categoryid);
            in_pubdate = view.findViewById(R.id.in_pubdate);
            in_pubby = view.findViewById(R.id.in_publishedby);
            in_title= view.findViewById(R.id.in_title);
            in_desc = view.findViewById(R.id.in_description);
            in_imageurl = view.findViewById(R.id.in_imageurl);
            
            in_image = view.findViewById(R.id.in_image);

        }
    }

    public NewsAdapter(Context context, List<NewsModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
        this.sess = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.MyViewHolder holder, int position) {
        NewsModel item = dmItems.get(position);

        holder.in_id.setText(String.valueOf(item.getId()));
        holder.in_categoryid.setText(item.getCategory_id());
        holder.in_pubdate.setText(item.getPublished_date());
        holder.in_pubby.setText(item.getPublished_by());
        holder.in_title.setText(item.getTitle());
        holder.in_desc.setText(item.getDescription());
        holder.in_imageurl.setText(item.getImage_url());

        if (sess.isData()) {
            if (!item.getImage_url().isEmpty() && !item.getImage_url().equals("null")) {
                Glide.with(context).load(item.getImage_url()).into(holder.in_image);
            } else {
                holder.in_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            }
        }else {
            holder.in_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<NewsModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
