package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.core.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 4/1/2018.
 */

public class AdAdapter extends RecyclerView.Adapter<AdAdapter.MyViewHolder> implements Filterable {

    private SessionManager sess;
    private List<AdvertisementModel> dmItems;
    private List<AdvertisementModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<AdvertisementModel> results = new ArrayList<AdvertisementModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final AdvertisementModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<AdvertisementModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ia_id, ia_title, ia_edition, ia_desc, ia_cost, ia_imageurl, ia_createdat;
        public ImageView ia_image;

        public MyViewHolder(View view) {
            super(view);
            ia_id = view.findViewById(R.id.ia_id);
            ia_title = view.findViewById(R.id.ia_title);
            ia_edition = view.findViewById(R.id.ia_edition);
            ia_desc = view.findViewById(R.id.ia_description);
            ia_cost = view.findViewById(R.id.ia_cost);
            ia_imageurl = view.findViewById(R.id.ia_imageurl);
            ia_createdat = view.findViewById(R.id.ia_createdat);

            ia_image = view.findViewById(R.id.ia_image);

        }
    }

    public AdAdapter(Context context, List<AdvertisementModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
        sess = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ad, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AdvertisementModel item = dmItems.get(position);

        holder.ia_id.setText(String.valueOf(item.getId()));
        holder.ia_title.setText(item.getTitle());
        holder.ia_edition.setText(item.getEdition());
        holder.ia_desc.setText(item.getWeb_url());
        holder.ia_cost.setText(item.getPrice());
        holder.ia_imageurl.setText(item.getImage_url());
        holder.ia_createdat.setText(item.getCreated_at());

        if(sess.isData()) {
            if (item.getImage_url().isEmpty() || item.getImage_url().equals("null")) {
                holder.ia_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            } else {
                Glide.with(context).load(item.getImage_url()).into(holder.ia_image);
            }
        }else{
            holder.ia_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setServices(List<AdvertisementModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
