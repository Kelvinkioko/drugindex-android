package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.FavouriteDao;
import com.dip.drugindex.api.db.models.FavouriteModel;
import com.dip.drugindex.api.db.models.FavouriteModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 5/10/2018.
 */


public class FavouriteRepository {

    private final FavouriteDao favouriteDao;

    public FavouriteRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        favouriteDao = db.favouriteDao();
    }

    public void addFavouriteItems(FavouriteModel favouriteModel){
        favouriteDao.insertFavourite(favouriteModel);
    }

    public LiveData<FavouriteModel> getFavouriteItems(int id){
        return favouriteDao.fetchFavouritebyId(id);
    }

    public LiveData<List<FavouriteModel>> getAllFavouriteItems(){
        return favouriteDao.loadFavouriteItems();
    }

    public void updateFavouriteItems(FavouriteModel favouriteModel){
        favouriteDao.updateFavourite(favouriteModel);
    }

    public void deleteFavouriteItems(FavouriteModel favouriteModel){
        favouriteDao.deleteFavourite(favouriteModel);
    }
}
