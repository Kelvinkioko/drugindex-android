package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.MarketModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

@Dao
public interface MarketDao {

    @Insert
    void insertMarket(MarketModel marketModel);

    @Query("SELECT * FROM market")
    LiveData<List<MarketModel>> loadMarketItems();

    @Query("SELECT * FROM market WHERE id = :id")
    LiveData<MarketModel> fetchMarketbyId (int id);

    @Query("SELECT * FROM market")
    MarketModel loadMarket();

    @Query("SELECT COUNT(id) FROM market")
    int countMarket();

    @Insert(onConflict = REPLACE)
    void updateMarket (MarketModel marketModel);

    @Delete
    void deleteMarket(MarketModel marketModel);
}
