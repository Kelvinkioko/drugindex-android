package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Entity(tableName = "drug")
public class DrugModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String drug_id, name, manufacturer;

    public DrugModel() {}

    public DrugModel(@NonNull int id, String drug_id, String name, String manufacturer) {
        this.id = id;
        this.drug_id = drug_id;
        this.name = name;
        this.manufacturer = manufacturer;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(String drug_id) {
        this.drug_id = drug_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
