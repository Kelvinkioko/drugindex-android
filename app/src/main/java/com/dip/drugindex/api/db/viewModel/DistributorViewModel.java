package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.DistributorDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.DistributorModel;
import com.dip.drugindex.api.db.models.IndexModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

public class DistributorViewModel extends AndroidViewModel {

    LiveData<List<DistributorModel>> liveDistributor;

    public DistributorViewModel (Application application) {
        super(application);
    }

    public LiveData<List<DistributorModel>> getAllDistributor(){
        try {
            liveDistributor = new fetchDistributorTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveDistributor;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchDistributorTask extends AsyncTask<Void, Void, LiveData<List<DistributorModel>>> {
        @Override
        protected LiveData<List<DistributorModel>> doInBackground(Void... voids){
            MutableLiveData<List<DistributorModel>> tempModel = new MutableLiveData<>();
            List<DistributorModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_DISTRIBUTOR, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONArray feedArray = new JSONArray(jsonString);
                    Log.e("Active", feedArray.toString());
                    if(feedArray.length() > 0){
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            DistributorModel item = new DistributorModel();
                            item.setDistributorid(feedObj.getString("distributor_id"));
                            item.setTitle(feedObj.getString("distributor_name"));

                            tempServiceModel.add(item);
                            tempModel.postValue(tempServiceModel);
                        }
                    }else{ }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }
    
}