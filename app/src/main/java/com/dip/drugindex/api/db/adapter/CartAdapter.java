package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.core.SessionManager;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> implements Filterable {

    private SessionManager sess;
    private List<CartModel> dmItems;
    private List<CartModel> orig;
    public Context context;
    public OnItemClickListener clickListener;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<CartModel> results = new ArrayList<CartModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final CartModel cd: orig){
                            if(cd.getName().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<CartModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setOnFeedItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface OnItemClickListener{
        void detailsClick(View view, int position);

        void optionsClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ic_id, ic_name, ic_final_cost, ic_cost, ic_quantity;
        public LinearLayout ic_more;
        public CircleImageView ic_picture;
        public ImageView ic_options;

        public MyViewHolder(View view) {
            super(view);
            ic_id = view.findViewById(R.id.ic_id);
            ic_name = view.findViewById(R.id.ic_name);
            ic_final_cost = view.findViewById(R.id.ic_final_cost);
            ic_cost = view.findViewById(R.id.ic_cost);
            ic_quantity = view.findViewById(R.id.ic_units);

            ic_picture = view.findViewById(R.id.ic_picture);
            ic_options = view.findViewById(R.id.ic_options);

            ic_more = view.findViewById(R.id.ic_more);

        }
    }

    public CartAdapter(Context context, List<CartModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
        sess = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartAdapter.MyViewHolder holder, int position) {
        CartModel item = dmItems.get(position);

        holder.ic_id.setText(item.getId());
        holder.ic_name.setText(item.getName());
        holder.ic_final_cost.setText(String.valueOf(Integer.parseInt(item.getCost())*Integer.parseInt(item.getQuantity())));
        holder.ic_cost.setText(item.getCost());
        holder.ic_quantity.setText(item.getQuantity());

        if (sess.isData()) {
            if (item.getImage().isEmpty() || item.getImage().equals("null")) {
                holder.ic_picture.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            } else {
                Glide.with(context).load(item.getImage()).into(holder.ic_picture);
            }
        }else{
            holder.ic_picture.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

        holder.ic_more.setOnClickListener(v -> clickListener.detailsClick(v, position));
        holder.ic_options.setOnClickListener(v -> clickListener.optionsClick(v, position));

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }
}
