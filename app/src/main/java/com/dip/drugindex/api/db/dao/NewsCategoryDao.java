package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.NewsCategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

@Dao
public interface NewsCategoryDao {

    @Insert
    void insertNewsCategory(NewsCategoryModel newsCategoryModel);

    @Query("SELECT * FROM newsCategory")
    LiveData<List<NewsCategoryModel>> loadNewsCategoryItems();

    @Query("SELECT * FROM newsCategory WHERE id = :id")
    LiveData<NewsCategoryModel> fetchNewsCategorybyId (int id);

    @Query("SELECT * FROM newsCategory")
    NewsCategoryModel loadNewsCategory();

    @Query("SELECT COUNT(id) FROM newsCategory")
    int countNewsCategory();

    @Insert(onConflict = REPLACE)
    void updateNewsCategory (NewsCategoryModel newsCategoryModel);

    @Delete
    void deleteNewsCategory(NewsCategoryModel newsCategoryModel);
}
