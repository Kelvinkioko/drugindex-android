package com.dip.drugindex.api.db.base;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.dip.drugindex.api.db.dao.ActiveDao;
import com.dip.drugindex.api.db.dao.AdvertisementDao;
import com.dip.drugindex.api.db.dao.BrandDao;
import com.dip.drugindex.api.db.dao.CartDao;
import com.dip.drugindex.api.db.dao.CategoryDao;
import com.dip.drugindex.api.db.dao.ClinicalDao;
import com.dip.drugindex.api.db.dao.DistributorDao;
import com.dip.drugindex.api.db.dao.DrugDao;
import com.dip.drugindex.api.db.dao.FavouriteDao;
import com.dip.drugindex.api.db.dao.ManufacturerDao;
import com.dip.drugindex.api.db.dao.MarketDao;
import com.dip.drugindex.api.db.dao.NewsCategoryDao;
import com.dip.drugindex.api.db.dao.NewsDao;
import com.dip.drugindex.api.db.dao.ResearchCategoryDao;
import com.dip.drugindex.api.db.dao.ResearchDao;
import com.dip.drugindex.api.db.dao.SearchDao;
import com.dip.drugindex.api.db.dao.SubcategoryDao;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.CategoryModel;
import com.dip.drugindex.api.db.models.ClinicalModel;
import com.dip.drugindex.api.db.models.DistributorModel;
import com.dip.drugindex.api.db.models.DrugModel;
import com.dip.drugindex.api.db.models.FavouriteModel;
import com.dip.drugindex.api.db.models.ManufacturerModel;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.db.models.SearchModel;
import com.dip.drugindex.api.db.models.SubcategoryModel;

/**
 * Created by Kelvin Kioko on 6/19/2018.
 */
@Database(entities = {
        ActiveModel.class,
        AdvertisementModel.class,
        BrandModel.class,
        CartModel.class,
        CategoryModel.class,
        ClinicalModel.class,
        DistributorModel.class,
        DrugModel.class,
        FavouriteModel.class,
        ManufacturerModel.class,
        MarketModel.class,
        NewsCategoryModel.class,
        NewsModel.class,
        ResearchCategoryModel.class,
        ResearchModel.class,
        SearchModel.class,
        SubcategoryModel.class},
        version = 1, exportSchema = false)
public abstract class DrugDatabase extends RoomDatabase {

    private static DrugDatabase INSTANCE;

    public abstract ActiveDao activeDao();
    public abstract AdvertisementDao advertisementDao();
    public abstract BrandDao brandDao();
    public abstract CartDao cartDao();
    public abstract CategoryDao categoryDao();
    public abstract ClinicalDao clinicalDao();
    public abstract DistributorDao distributorDao();
    public abstract DrugDao drugDao();
    public abstract FavouriteDao favouriteDao();
    public abstract ManufacturerDao manufacturerDao();
    public abstract MarketDao marketDao();
    public abstract NewsCategoryDao newsCategoryDao();
    public abstract NewsDao newsDao();
    public abstract ResearchCategoryDao researchCategoryDao();
    public abstract ResearchDao researchDao();
    public abstract SearchDao searchDao();
    public abstract SubcategoryDao subcategoryDao();

    public static DrugDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DrugDatabase.class, "drug_db")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
