package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.DrugModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Dao
public interface DrugDao {

    @Insert
    void insertDrug(DrugModel drugModel);

    @Query("SELECT * FROM drug")
    LiveData<List<DrugModel>> loadDrugItems();

    @Query("SELECT * FROM drug WHERE id = :id")
    LiveData<DrugModel> fetchDrugbyId (int id);

    @Query("SELECT * FROM drug")
    DrugModel loadDrug();

    @Query("SELECT COUNT(id) FROM drug")
    int countDrug();

    @Insert(onConflict = REPLACE)
    void updateDrug (DrugModel drugModel);

    @Delete
    void deleteDrug(DrugModel drugModel);
}
