package com.dip.drugindex.api.server;

/**
 * Created by Kelvin Kioko on 2/27/2018.
 */

public interface UrlConfig {

    String constant = "http://drugindex.allthingzcode.com/api/";

    String URL_REGISTER = constant + "register";

    String URL_RECOVER = constant + "recover";

    String URL_LOGIN = constant + "login";


    String URL_BRANDS = constant + "brands";

    String URL_ACTIVEING = constant + "active-ingridients";

    String URL_DRUGS = constant + "drugs";

    String URL_CATEGORY = constant + "category";

    String URL_SUBCATEGORY = constant + "sub-category";

    String URL_MANUFACTURER = constant + "manufacturer";

    String URL_DISTRIBUTOR = constant + "distributor";


    String URL_RESEARCH_DATABASE = constant + "research-database";

    String URL_RESEARCH_CATEGORY = constant + "research-category";

    String URL_RESEARCH_BY_CATEGORY = constant + "research-category-details";

    String URL_UPDATE_PIC = constant + "update-profile-pic";
//    {user_id}{result, user, message}
    String URL_UPDATE_PROFILE = constant + "update-profile";
//    {name,phone_number, user_id}{result, user, message}

    String URL_MARKET_PLACE = constant + "market-place";
//    {result,market_place}
    String URL_NEWS_CATEGORY = constant + "news-category";
//    {result,news_category}
    String URL_NEWS = constant + "news";
//    {result, news}
    String URL_NEWS_BY_CATEGORY = constant + "news-by-category";
//    {category_id}{result, news}

    String URL_ACTIVE = constant + "active-details";
    //    {result, active-details}
    String URL_DRUG_DETAILS = constant + "drug-details";
    //    {result, active-details}
}
