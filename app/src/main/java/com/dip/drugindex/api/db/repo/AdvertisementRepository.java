package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.AdvertisementDao;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.db.models.AdvertisementModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 5/7/2018.
 */

public class AdvertisementRepository {

    private final AdvertisementDao advertisementDao;

    public AdvertisementRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        advertisementDao = db.advertisementDao();
    }

    public void addAdvertisementItems(AdvertisementModel advertisementModel){
        advertisementDao.insertAdvertisement(advertisementModel);
    }

    public LiveData<AdvertisementModel> getAdvertisementItems(int id){
        return advertisementDao.fetchAdvertisementbyId(id);
    }

    public LiveData<List<AdvertisementModel>> getAllAdvertisementItems(){
        return advertisementDao.loadAdvertisementItems();
    }

    public void updateAdvertisementItems(AdvertisementModel advertisementModel){
        advertisementDao.updateAdvertisement(advertisementModel);
    }

    public void deleteAdvertisementItems(AdvertisementModel advertisementModel){
        advertisementDao.deleteAdvertisement(advertisementModel);
    }
}