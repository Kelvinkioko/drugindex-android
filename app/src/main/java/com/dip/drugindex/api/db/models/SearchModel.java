package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 4/20/2018.
 */

@Entity(tableName = "search")
public class SearchModel {

    @NonNull
    @PrimaryKey
    public String id;
    public String drug, activeing;

    public SearchModel() {}

    public SearchModel(@NonNull String id, String drug, String activeing) {
        this.id = id;
        this.drug = drug;
        this.activeing = activeing;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getActiveing() {
        return activeing;
    }

    public void setActiveing(String activeing) {
        this.activeing = activeing;
    }
}
