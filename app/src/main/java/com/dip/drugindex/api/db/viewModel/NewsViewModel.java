package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.NewsDao;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsViewModel extends AndroidViewModel {

    LiveData<List<NewsModel>> liveNews;

    public NewsViewModel (Application application) {
        super(application);
    }

    public LiveData<List<NewsModel>> getAllNews(){
        try {
            liveNews = new fetchNewsTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveNews;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchNewsTask extends AsyncTask<Void, Void, LiveData<List<NewsModel>>> {
        @Override
        protected LiveData<List<NewsModel>> doInBackground(Void... voids){
            MutableLiveData<List<NewsModel>> tempModel = new MutableLiveData<>();
            List<NewsModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_NEWS, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());

                    JSONArray feedArray = jObj.getJSONArray("news");

                    if(feedArray.length() > 0){
                        JSONObject unoObj = (JSONObject) feedArray.get(0);
//                        nr_id.setText(unoObj.getString("id"));
//                        nr_catid.setText(unoObj.getString("category_id"));
//                        nr_date.setText(unoObj.getString("published_date"));
//                        nr_publisher.setText(unoObj.getString("published_by"));
//                        nr_title.setText(unoObj.getString("title"));
//                        nr_desc.setText(Html.fromHtml(unoObj.getString("description")).toString());
//                        nr_imageurl.setText(unoObj.getString("image_url"));
//
//                        if(unoObj.getString("image_url").isEmpty() || unoObj.getString("image_url").equals("null")){
//                            nr_image.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.avatar));
//                        }else{
//                            Glide.with(getActivity()).load(unoObj.getString("image_url")).into(nr_image);
//                        }

                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            NewsModel item = new NewsModel();
                            item.setId(feedObj.getInt("id"));
                            item.setCategory_id(feedObj.getString("category_id"));
                            item.setPublished_date(feedObj.getString("published_date"));
                            item.setPublished_by(feedObj.getString("published_by"));
                            item.setTitle(feedObj.getString("title"));
                            item.setDescription(feedObj.getString("description"));
                            item.setImage_url(feedObj.getString("image_url"));

                            tempServiceModel.add(item);
                            tempModel.postValue(tempServiceModel);
                        }

                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }
}
