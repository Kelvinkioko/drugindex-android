package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ClinicalDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.ClinicalModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

public class ClinicalViewModel extends AndroidViewModel {

    LiveData<List<ClinicalModel>> liveClinical;

    public ClinicalViewModel (Application application) {
        super(application);
    }

    public LiveData<List<ClinicalModel>> getAllClinical(){
        try {
            liveClinical = new fetchClinicalTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveClinical;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchClinicalTask extends AsyncTask<Void, Void, LiveData<List<ClinicalModel>>> {
        @Override
        protected LiveData<List<ClinicalModel>> doInBackground(Void... voids){
            MutableLiveData<List<ClinicalModel>> tempModel = new MutableLiveData<>();
            List<ClinicalModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_ACTIVEING, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("clinical_ingridients");

                        if(feedArray.length() > 0){
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                ClinicalModel item = new ClinicalModel();
                                item.setId(feedObj.getInt("clinical_id"));
                                item.setName(feedObj.getString("clinical_name"));

                                tempServiceModel.add(item);
                                tempModel.postValue(tempServiceModel);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }

}