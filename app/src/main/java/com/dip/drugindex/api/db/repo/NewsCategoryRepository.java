package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.NewsCategoryDao;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.api.db.models.NewsCategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsCategoryRepository {

    private final NewsCategoryDao newsCategoryDao;

    public NewsCategoryRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        newsCategoryDao = db.newsCategoryDao();
    }

    public void addNewsCategoryItems(NewsCategoryModel newsCategoryModel){
        newsCategoryDao.insertNewsCategory(newsCategoryModel);
    }

    public LiveData<NewsCategoryModel> getNewsCategoryItems(int id){
        return newsCategoryDao.fetchNewsCategorybyId(id);
    }

    public LiveData<List<NewsCategoryModel>> getAllNewsCategoryItems(){
        return newsCategoryDao.loadNewsCategoryItems();
    }

    public void updateNewsCategoryItems(NewsCategoryModel newsCategoryModel){
        newsCategoryDao.updateNewsCategory(newsCategoryModel);
    }

    public void deleteNewsCategoryItems(NewsCategoryModel newsCategoryModel){
        newsCategoryDao.deleteNewsCategory(newsCategoryModel);
    }
}
