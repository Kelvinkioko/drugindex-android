package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Entity(tableName = "category")
public class CategoryModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String categoryid, title;

    public CategoryModel() {}

    public CategoryModel(@NonNull int id, String categoryid, String title) {
        this.id = id;
        this.categoryid = categoryid;
        this.title = title;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}