package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.ResearchModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchAdapter extends RecyclerView.Adapter<ResearchAdapter.MyViewHolder> implements Filterable {

    private List<ResearchModel> dmItems;
    private List<ResearchModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<ResearchModel> results = new ArrayList<ResearchModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final ResearchModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<ResearchModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ir_category_name, ir_title, ir_published_by, ir_url, ir_image_url, ir_page_count,
                ir_topics, ir_price, ir_date_published;

        public MyViewHolder(View view) {
            super(view);
            ir_category_name = view.findViewById(R.id.ir_category);
            ir_title = view.findViewById(R.id.ir_title);
            ir_published_by = view.findViewById(R.id.ir_publisher);
            ir_url = view.findViewById(R.id.ir_url);
            ir_image_url = view.findViewById(R.id.ir_image);
            ir_page_count = view.findViewById(R.id.ir_pagecount);
            ir_topics = view.findViewById(R.id.ir_tags);
            ir_price = view.findViewById(R.id.ir_cost);
            ir_date_published = view.findViewById(R.id.ir_published);

        }
    }

    public ResearchAdapter(Context context, List<ResearchModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_research, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ResearchModel item = dmItems.get(position);

        holder.ir_category_name.setText(item.getCategory_name());
        holder.ir_title.setText(item.getTitle());
        holder.ir_published_by.setText(item.getPublished_by());
        holder.ir_url.setText(item.getUrl());
        holder.ir_image_url.setText(item.getImage_url());
        holder.ir_page_count.setText(item.getPage_count());
        holder.ir_topics.setText(item.getTopics());
        holder.ir_price.setText(item.getPrice());
        holder.ir_date_published.setText(item.getDate_published());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<ResearchModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
