package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.ResearchCategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

@Dao
public interface ResearchCategoryDao {

    @Insert
    void insertResearchCategory(ResearchCategoryModel researchCategoryModel);

    @Query("SELECT * FROM researchCategory")
    LiveData<List<ResearchCategoryModel>> loadResearchCategoryItems();

    @Query("SELECT * FROM researchCategory WHERE id = :id")
    LiveData<ResearchCategoryModel> fetchResearchCategorybyId (int id);

    @Query("SELECT * FROM researchCategory")
    ResearchCategoryModel loadResearchCategory();

    @Query("SELECT COUNT(id) FROM researchCategory")
    int countResearchCategory();

    @Insert(onConflict = REPLACE)
    void updateResearchCategory (ResearchCategoryModel researchCategoryModel);

    @Delete
    void deleteResearchCategory(ResearchCategoryModel researchCategoryModel);
}