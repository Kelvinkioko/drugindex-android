package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.ActiveModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Dao
public interface ActiveDao {

    @Insert
    void insertActive(ActiveModel activeModel);

    @Query("SELECT * FROM active")
    LiveData<List<ActiveModel>> loadActiveItems();

    @Query("SELECT * FROM active WHERE id = :id")
    LiveData<ActiveModel> fetchActivebyId (int id);

    @Query("SELECT * FROM active")
    ActiveModel loadActive();

    @Query("SELECT COUNT(id) FROM active")
    int countActive();

    @Insert(onConflict = REPLACE)
    void updateActive (ActiveModel activeModel);

    @Delete
    void deleteActive(ActiveModel activeModel);
}