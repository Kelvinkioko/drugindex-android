package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Entity(tableName = "brand")
public class BrandModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String brandid,  brand, manufacturer;

    public BrandModel() {}

    public BrandModel(@NonNull int id, String brandid, String brand, String manufacturer) {
        this.id = id;
        this.brandid = brandid;
        this.brand = brand;
        this.manufacturer = manufacturer;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
