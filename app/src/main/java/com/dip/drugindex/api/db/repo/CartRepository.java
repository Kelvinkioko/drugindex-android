package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.CartDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.CartModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

public class CartRepository {

    private final CartDao cartDao;

    public CartRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        cartDao = db.cartDao();
    }

    public void addCartItems(CartModel cartModel){
        cartDao.insertCart(cartModel);
    }

    public LiveData<CartModel> getCartItems(int id){
        return cartDao.fetchCartbyId(id);
    }

    public LiveData<List<CartModel>> getAllCartItems(){
        return cartDao.loadCartItems();
    }

    public void updateCartItems(CartModel cartModel){
        cartDao.updateCart(cartModel);
    }

    public void deleteCartItems(CartModel cartModel){
        cartDao.deleteCart(cartModel);
    }
}