package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.db.models.SearchModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class ActiveAdapter extends RecyclerView.Adapter<ActiveAdapter.MyViewHolder> implements Filterable {

    private List<ActiveModel> dmItems;
    private List<ActiveModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<ActiveModel> results = new ArrayList<ActiveModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final ActiveModel cd: orig){
                            if(cd.getActive().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<ActiveModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ia_id, ia_drug, ia_active;

        public MyViewHolder(View view) {
            super(view);
            ia_id = view.findViewById(R.id.ia_id);
            ia_drug = view.findViewById(R.id.ia_drug);
            ia_active = view.findViewById(R.id.ia_active);
        }
    }

    public ActiveAdapter(Context context, List<ActiveModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_active, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ActiveModel item = dmItems.get(position);

        holder.ia_id.setText(item.getDrugid());
        holder.ia_drug.setText(item.getDrug());
        holder.ia_active.setText(item.getActive());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<ActiveModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
