package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

@Entity(tableName = "market")
public class MarketModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String title, edition, price, description, image_url, created_at;

    public MarketModel() {}

    public MarketModel(@NonNull int id, String title, String edition, String price, String description, String image_url, String created_at) {
        this.id = id;
        this.title = title;
        this.edition = edition;
        this.price = price;
        this.description = description;
        this.image_url = image_url;
        this.created_at = created_at;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
