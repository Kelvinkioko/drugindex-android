package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.MarketDao;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */


public class MarketViewModel extends AndroidViewModel {

    LiveData<List<MarketModel>> liveMarket;

    public MarketViewModel (Application application) {
        super(application);
    }

    public LiveData<List<MarketModel>> getAllMarket(){
        try {
            liveMarket = new fetchMarketTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveMarket;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchMarketTask extends AsyncTask<Void, Void, LiveData<List<MarketModel>>> {
        @Override
        protected LiveData<List<MarketModel>> doInBackground(Void... voids){
            MutableLiveData<List<MarketModel>> tempModel = new MutableLiveData<>();
            List<MarketModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_MARKET_PLACE, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("market_place");

                        if (feedArray.length() > 0) {

                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                MarketModel item = new MarketModel();
                                item.setId(feedObj.getInt("id"));
                                item.setTitle(feedObj.getString("title"));
                                item.setEdition(feedObj.getString("edition") + " Edition");
                                item.setPrice(feedObj.getString("price"));
                                item.setDescription(Html.fromHtml(feedObj.getString("description")).toString());
                                item.setImage_url(feedObj.getString("image_url"));
                                item.setCreated_at(feedObj.getString("created_at"));

                                tempServiceModel.add(item);
                                tempModel.postValue(tempServiceModel);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }

}
