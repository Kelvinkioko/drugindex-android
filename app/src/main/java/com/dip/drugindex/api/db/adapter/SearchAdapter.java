package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.SearchModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 4/20/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> implements Filterable {

    private List<SearchModel> dmItems;
    private List<SearchModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<SearchModel> results = new ArrayList<>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final SearchModel cd: orig){
                            if(cd.getDrug().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<SearchModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView is_id, is_drug, is_activeing, is_initial;

        public MyViewHolder(View view) {
            super(view);
            is_id = view.findViewById(R.id.is_id);
            is_drug = view.findViewById(R.id.is_drug);
            is_activeing = view.findViewById(R.id.is_activeing);
            is_initial = view.findViewById(R.id.is_initial);
        }
    }

    public SearchAdapter(Context context, List<SearchModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searches, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SearchModel item = dmItems.get(position);

        holder.is_id.setText(String.valueOf(item.getId()));
        holder.is_drug.setText(item.getDrug());
        holder.is_activeing.setText(item.getActiveing());
        String ini = item.getDrug();
        holder.is_initial.setText(String.valueOf(ini.charAt(0)));

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setSearch(List<SearchModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
