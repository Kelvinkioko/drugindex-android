package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ResearchDao;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchViewModel extends AndroidViewModel {

    LiveData<List<ResearchModel>> liveResearch;

    public ResearchViewModel (Application application) {
        super(application);
    }

    public LiveData<List<ResearchModel>> getAllResearch(){
        try {
            liveResearch = new fetchResearchTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveResearch;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchResearchTask extends AsyncTask<Void, Void, LiveData<List<ResearchModel>>> {
        @Override
        protected LiveData<List<ResearchModel>> doInBackground(Void... voids){
            MutableLiveData<List<ResearchModel>> tempModel = new MutableLiveData<>();
            List<ResearchModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_RESEARCH_DATABASE, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jObj = new JSONObject(jsonString);

                    JSONArray feedArray = jObj.getJSONArray("research");

                    Log.e("Research", feedArray.toString());
                    if(feedArray.length() > 0){
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            ResearchModel item = new ResearchModel();;
                            item.setCategory_name(feedObj.getString("category_name"));
                            item.setTitle(feedObj.getString("title"));
                            item.setPublished_by(feedObj.getString("published_by"));
                            item.setUrl(feedObj.getString("url"));
                            item.setImage_url(feedObj.getString("image_url"));
                            item.setPage_count(feedObj.getString("page_count"));
                            item.setTopics(feedObj.getString("topics"));
                            item.setPrice(feedObj.getString("price"));
                            item.setDate_published(feedObj.getString("date_published"));

                            tempServiceModel.add(item);
                            tempModel.postValue(tempServiceModel);
                        }
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }

}
