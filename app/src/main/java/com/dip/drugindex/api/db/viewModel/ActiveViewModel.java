package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ActiveDao;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class ActiveViewModel extends AndroidViewModel {

    LiveData<List<ActiveModel>> liveActive;

    public ActiveViewModel (Application application) {
        super(application);
    }

    public LiveData<List<ActiveModel>> getAllActive(){
        try {
            liveActive = new fetchActiveTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveActive;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchActiveTask extends AsyncTask<Void, Void, LiveData<List<ActiveModel>>> {
        @Override
        protected LiveData<List<ActiveModel>> doInBackground(Void... voids){
            MutableLiveData<List<ActiveModel>> tempModel = new MutableLiveData<>();
            List<ActiveModel> tempActiveModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_ACTIVEING, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("active_ingridients");

                        if(feedArray.length() > 0){
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                ActiveModel item = new ActiveModel();
                                item.setDrugid(feedObj.getString("active_id"));
                                item.setDrug("Not Specified");
                                item.setActive(feedObj.getString("active_name"));

                                tempActiveModel.add(item);
                                tempModel.postValue(tempActiveModel);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);
            return tempModel;
        }
    }

}