package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Entity(tableName = "manufacturer")
public class ManufacturerModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String manufactureid, title;

    public ManufacturerModel() {}

    public ManufacturerModel(@NonNull int id, String manufactureid, String title) {
        this.id = id;
        this.manufactureid = manufactureid;
        this.title = title;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getManufactureid() {
        return manufactureid;
    }

    public void setManufactureid(String manufactureid) {
        this.manufactureid = manufactureid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
