package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.ManufacturerModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Dao
public interface ManufacturerDao {

    @Insert
    void insertManufacturer(ManufacturerModel manufacturerModel);

    @Query("SELECT * FROM manufacturer")
    LiveData<List<ManufacturerModel>> loadManufacturerItems();

    @Query("SELECT * FROM manufacturer WHERE id = :id")
    LiveData<ManufacturerModel> fetchManufacturerbyId (int id);

    @Query("SELECT * FROM manufacturer")
    ManufacturerModel loadManufacturer();

    @Query("SELECT COUNT(id) FROM manufacturer")
    int countManufacturer();

    @Insert(onConflict = REPLACE)
    void updateManufacturer (ManufacturerModel manufacturerModel);

    @Delete
    void deleteManufacturer(ManufacturerModel manufacturerModel);
}
