package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.DrugDao;
import com.dip.drugindex.api.db.dao.DrugDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.DrugModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */


public class DrugRepository {

    private final DrugDao drugDao;

    public DrugRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        drugDao = db.drugDao();
    }

    public void addDrugItems(DrugModel drugModel){
        drugDao.insertDrug(drugModel);
    }

    public LiveData<DrugModel> getDrugItems(int id){
        return drugDao.fetchDrugbyId(id);
    }

    public LiveData<List<DrugModel>> getAllDrugItems(){
        return drugDao.loadDrugItems();
    }

    public void updateDrugItems(DrugModel drugModel){
        drugDao.updateDrug(drugModel);
    }

    public void deleteDrugItems(DrugModel drugModel){
        drugDao.deleteDrug(drugModel);
    }
}
