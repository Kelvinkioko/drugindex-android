package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.SearchModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/20/2018.
 */

@Dao
public interface SearchDao {

    @Insert
    void insertSearch(SearchModel searchModel);

    @Query("SELECT * FROM search")
    LiveData<List<SearchModel>> loadSearchItems();

    @Query("SELECT * FROM search WHERE id = :id")
    LiveData<SearchModel> fetchSearchbyId (int id);

    @Query("SELECT * FROM search")
    SearchModel loadSearch();

    @Query("SELECT COUNT(id) FROM search")
    int countSearch();

    @Insert(onConflict = REPLACE)
    void updateSearch (SearchModel searchModel);

    @Delete
    void deleteSearch(SearchModel searchModel);
}