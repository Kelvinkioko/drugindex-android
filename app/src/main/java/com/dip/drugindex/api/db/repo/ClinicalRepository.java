package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ClinicalDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.ClinicalModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

public class ClinicalRepository {

    private final ClinicalDao clinicalDao;

    public ClinicalRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        clinicalDao = db.clinicalDao();
    }

    public void addClinicalItems(ClinicalModel clinicalModel){
        clinicalDao.insertClinical(clinicalModel);
    }

    public LiveData<ClinicalModel> getClinicalItems(int id){
        return clinicalDao.fetchClinicalbyId(id);
    }

    public LiveData<List<ClinicalModel>> getAllClinicalItems(){
        return clinicalDao.loadClinicalItems();
    }

    public void updateClinicalItems(ClinicalModel clinicalModel){
        clinicalDao.updateClinical(clinicalModel);
    }

    public void deleteClinicalItems(ClinicalModel clinicalModel){
        clinicalDao.deleteClinical(clinicalModel);
    }
}