package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

@Entity(tableName = "cart")
public class CartModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String name, image, cost, quantity;

    public CartModel() {}

    public CartModel(@NonNull int id, String name, String image, String cost, String quantity) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.cost = cost;
        this.quantity = quantity;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}