package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.BrandDao;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.BrandModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */


public class BrandRepository {

    private final BrandDao brandDao;

    public BrandRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        brandDao = db.brandDao();
    }

    public void addBrandItems(BrandModel brandModel){
        brandDao.insertBrand(brandModel);
    }

    public LiveData<BrandModel> getBrandItems(int id){
        return brandDao.fetchBrandbyId(id);
    }

    public LiveData<List<BrandModel>> getAllBrandItems(){
        return brandDao.loadBrandItems();
    }

    public void updateBrandItems(BrandModel brandModel){
        brandDao.updateBrand(brandModel);
    }

    public void deleteBrandItems(BrandModel brandModel){
        brandDao.deleteBrand(brandModel);
    }
}