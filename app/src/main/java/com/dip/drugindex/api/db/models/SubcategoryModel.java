package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Entity(tableName = "subcategory")
public class SubcategoryModel {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String subid, title;

    public SubcategoryModel() {}

    public SubcategoryModel(@NonNull int id, String subid, String title) {
        this.id = id;
        this.subid = subid;
        this.title = title;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
