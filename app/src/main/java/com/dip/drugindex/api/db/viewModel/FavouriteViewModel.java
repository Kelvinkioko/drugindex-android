package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.FavouriteDao;
import com.dip.drugindex.api.db.models.FavouriteModel;
import com.dip.drugindex.api.db.models.FavouriteModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 5/10/2018.
 */


public class FavouriteViewModel extends AndroidViewModel {

    LiveData<List<FavouriteModel>> liveFavourite;

    public FavouriteViewModel (Application application) {
        super(application);
    }

    public LiveData<List<FavouriteModel>> getAllFavourite(){
        try {
            liveFavourite = new fetchFavouriteTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveFavourite;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchFavouriteTask extends AsyncTask<Void, Void, LiveData<List<FavouriteModel>>> {
        @Override
        protected LiveData<List<FavouriteModel>> doInBackground(Void... voids){
            MutableLiveData<List<FavouriteModel>> tempModel = new MutableLiveData<>();
            List<FavouriteModel> tempServiceModel = new ArrayList<>();

//            TODO: Volley request to finally fetch all services provided and store them in live data for the UI to consume

            return tempModel;
        }
    }

}
