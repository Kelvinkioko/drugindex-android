package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.ClinicalModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

@Dao
public interface ClinicalDao {

    @Insert
    void insertClinical(ClinicalModel clinicalModel);

    @Query("SELECT * FROM clinical")
    LiveData<List<ClinicalModel>> loadClinicalItems();

    @Query("SELECT * FROM clinical WHERE id = :id")
    LiveData<ClinicalModel> fetchClinicalbyId (int id);

    @Query("SELECT * FROM clinical")
    ClinicalModel loadClinical();

    @Query("SELECT COUNT(id) FROM clinical")
    int countClinical();

    @Insert(onConflict = REPLACE)
    void updateClinical (ClinicalModel clinicalModel);

    @Delete
    void deleteClinical(ClinicalModel clinicalModel);
}