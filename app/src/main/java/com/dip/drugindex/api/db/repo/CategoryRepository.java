package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.CategoryDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.CategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

public class CategoryRepository {

    private final CategoryDao categoryDao;

    public CategoryRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        categoryDao = db.categoryDao();
    }

    public void addCategoryItems(CategoryModel categoryModel){
        categoryDao.insertCategory(categoryModel);
    }

    public LiveData<CategoryModel> getCategoryItems(int id){
        return categoryDao.fetchCategorybyId(id);
    }

    public LiveData<List<CategoryModel>> getAllCategoryItems(){
        return categoryDao.loadCategoryItems();
    }

    public void updateCategoryItems(CategoryModel categoryModel){
        categoryDao.updateCategory(categoryModel);
    }

    public void deleteCategoryItems(CategoryModel categoryModel){
        categoryDao.deleteCategory(categoryModel);
    }
}