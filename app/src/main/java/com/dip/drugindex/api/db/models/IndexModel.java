package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Entity(tableName = "index")
public class IndexModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String title;

    public IndexModel() {}

    public IndexModel(@NonNull int id, String title) {
        this.id = id;
        this.title = title;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
