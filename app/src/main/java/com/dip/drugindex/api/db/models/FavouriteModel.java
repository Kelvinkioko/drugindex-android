package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 5/10/2018.
 */

@Entity(tableName = "favourite")
public class FavouriteModel {

    @NonNull
    @PrimaryKey
    public String id;
    public String title, desc, postby, imageurl, postdate;

    public FavouriteModel() {}

    public FavouriteModel(@NonNull String id, String title, String desc, String postby, String imageurl, String postdate) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.postby = postby;
        this.imageurl = imageurl;
        this.postdate = postdate;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPostby() {
        return postby;
    }

    public void setPostby(String postby) {
        this.postby = postby;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }
}
