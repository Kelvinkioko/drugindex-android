package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

@Entity(tableName = "clinical")
public class ClinicalModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String name;

    public ClinicalModel() {}

    public ClinicalModel(@NonNull int id, String name) {
        this.id = id;
        this.name = name;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}