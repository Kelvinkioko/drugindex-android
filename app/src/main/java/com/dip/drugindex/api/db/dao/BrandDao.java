package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.BrandModel;
import com.dip.drugindex.api.db.models.BrandModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Dao
public interface BrandDao {

    @Insert
    void insertBrand(BrandModel brandModel);

    @Query("SELECT * FROM brand")
    LiveData<List<BrandModel>> loadBrandItems();

    @Query("SELECT * FROM brand WHERE id = :id")
    LiveData<BrandModel> fetchBrandbyId (int id);

    @Query("SELECT * FROM brand")
    BrandModel loadBrand();

    @Query("SELECT COUNT(id) FROM brand")
    int countBrand();

    @Insert(onConflict = REPLACE)
    void updateBrand (BrandModel brandModel);

    @Delete
    void deleteBrand(BrandModel brandModel);
}
