package com.dip.drugindex.api.server;

import android.view.View;

/**
 * Created by Kelvin Kioko on 3/9/2018.
 */

public interface RecyclerClick {
    void onClick(View view, int position);
}
