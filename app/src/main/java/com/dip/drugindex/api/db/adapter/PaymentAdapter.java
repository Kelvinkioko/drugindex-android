package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.PaymentModel;

import java.util.List;

/**
 * Created by Kelvin Kioko on 3/25/2018.
 */

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder>{

    private List<PaymentModel> dmItems;
    public Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ip_type, ip_key;
        public ImageView ip_picture;

        public MyViewHolder(View view) {
            super(view);
            ip_type = view.findViewById(R.id.ip_type);
            ip_key = view.findViewById(R.id.ip_key);

            ip_picture = view.findViewById(R.id.ip_picture);

        }
    }

    public PaymentAdapter(Context context, List<PaymentModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PaymentAdapter.MyViewHolder holder, int position) {
        PaymentModel item = dmItems.get(position);

        holder.ip_type.setText(item.getType());
        holder.ip_key.setText(item.getKey());

        if(item.getType().equals("mpesa")){
            holder.ip_picture.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }else if(item.getType().equals("paypal")){
            holder.ip_picture.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }
}
