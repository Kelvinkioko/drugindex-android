package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.CartDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.CartModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

public class CartViewModel extends AndroidViewModel {

    LiveData<List<CartModel>> liveCart;

    public CartViewModel (Application application) {
        super(application);
    }

    public LiveData<List<CartModel>> getAllCart(){
        try {
            liveCart = new fetchCartTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveCart;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchCartTask extends AsyncTask<Void, Void, LiveData<List<CartModel>>> {
        @Override
        protected LiveData<List<CartModel>> doInBackground(Void... voids){
            MutableLiveData<List<CartModel>> tempModel = new MutableLiveData<>();
            List<CartModel> tempServiceModel = new ArrayList<>();

//            TODO: Volley request to finally fetch all services provided and store them in live data for the UI to consume

            return tempModel;
        }
    }

}