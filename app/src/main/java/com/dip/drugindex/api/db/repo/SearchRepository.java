package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.SearchDao;
import com.dip.drugindex.api.db.models.SearchModel;
import com.dip.drugindex.api.db.models.SearchModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/20/2018.
 */

public class SearchRepository {

    private final SearchDao searchDao;

    public SearchRepository(Application application) {
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        searchDao = db.searchDao();
    }

    public void addSearchItems(SearchModel searchModel) {
        searchDao.insertSearch(searchModel);
    }

    public LiveData<SearchModel> getSearchItems(int id) {
        return searchDao.fetchSearchbyId(id);
    }

    public LiveData<List<SearchModel>> getAllSearchItems() {
        return searchDao.loadSearchItems();
    }

    public void updateSearchItems(SearchModel searchModel) {
        searchDao.updateSearch(searchModel);
    }

    public void deleteSearchItems(SearchModel searchModel) {
        searchDao.deleteSearch(searchModel);
    }
}