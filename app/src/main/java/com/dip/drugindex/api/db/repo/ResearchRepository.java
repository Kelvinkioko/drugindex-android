package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ResearchDao;
import com.dip.drugindex.api.db.models.ResearchModel;
import com.dip.drugindex.api.db.models.ResearchModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchRepository {

    private final ResearchDao researchDao;

    public ResearchRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        researchDao = db.researchDao();
    }

    public void addResearchItems(ResearchModel researchModel){
        researchDao.insertResearch(researchModel);
    }

    public LiveData<ResearchModel> getResearchItems(int id){
        return researchDao.fetchResearchbyId(id);
    }

    public LiveData<List<ResearchModel>> getAllResearchItems(){
        return researchDao.loadResearchItems();
    }

    public void updateResearchItems(ResearchModel researchModel){
        researchDao.updateResearch(researchModel);
    }

    public void deleteResearchItems(ResearchModel researchModel){
        researchDao.deleteResearch(researchModel);
    }
}
