package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.DistributorDao;
import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.DistributorModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

public class DistributorRepository {

    private final DistributorDao distributorDao;

    public DistributorRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        distributorDao = db.distributorDao();
    }

    public void addDistributorItems(DistributorModel distributorModel){
        distributorDao.insertDistributor(distributorModel);
    }

    public LiveData<DistributorModel> getDistributorItems(int id){
        return distributorDao.fetchDistributorbyId(id);
    }

    public LiveData<List<DistributorModel>> getAllDistributorItems(){
        return distributorDao.loadDistributorItems();
    }

    public void updateDistributorItems(DistributorModel distributorModel){
        distributorDao.updateDistributor(distributorModel);
    }

    public void deleteDistributorItems(DistributorModel distributorModel){
        distributorDao.deleteDistributor(distributorModel);
    }
}