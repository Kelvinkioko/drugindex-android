package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.AdvertisementDao;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 5/7/2018.
 */

public class AdvertisementViewModel extends AndroidViewModel {

    LiveData<List<AdvertisementModel>> liveAdvertisement;

    public AdvertisementViewModel (Application application) {
        super(application);
    }

    public LiveData<List<AdvertisementModel>> getAllAdvertisement(){
        try {
            liveAdvertisement = new fetchAdvertisementTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveAdvertisement;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchAdvertisementTask extends AsyncTask<Void, Void, LiveData<List<AdvertisementModel>>> {
        @Override
        protected LiveData<List<AdvertisementModel>> doInBackground(Void... voids){
            MutableLiveData<List<AdvertisementModel>> tempModel = new MutableLiveData<>();
            List<AdvertisementModel> tempAdvertModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_MARKET_PLACE, response -> {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("market_place");

                        if (feedArray.length() > 0) {
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                AdvertisementModel item = new AdvertisementModel();
                                item.setId(feedObj.getInt("id"));
                                item.setTitle(feedObj.getString("title"));
                                item.setEdition(feedObj.getString("edition") + " Edition");
                                item.setPrice(feedObj.getString("price"));
                                item.setWeb_url("https://www.google.com");
                                item.setImage_url(feedObj.getString("image_url"));
                                item.setCreated_at(feedObj.getString("created_at"));

                                tempAdvertModel.add(item);
                                tempModel.postValue(tempAdvertModel);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }
    
}