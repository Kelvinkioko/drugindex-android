package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.SubcategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */
@Dao
public interface SubcategoryDao {

    @Insert
    void insertSubcategory(SubcategoryModel subcategoryModel);

    @Query("SELECT * FROM subcategory")
    LiveData<List<SubcategoryModel>> loadSubcategoryItems();

    @Query("SELECT * FROM subcategory WHERE id = :id")
    LiveData<SubcategoryModel> fetchSubcategorybyId (int id);

    @Query("SELECT * FROM subcategory")
    SubcategoryModel loadSubcategory();

    @Query("SELECT COUNT(id) FROM subcategory")
    int countSubcategory();

    @Insert(onConflict = REPLACE)
    void updateSubcategory (SubcategoryModel subcategoryModel);

    @Delete
    void deleteSubcategory(SubcategoryModel subcategoryModel);
}
