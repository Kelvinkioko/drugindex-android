package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.CartModel;
import com.dip.drugindex.api.db.models.CategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Dao
public interface CategoryDao {

    @Insert
    void insertCategory(CategoryModel cartModel);

    @Query("SELECT * FROM category")
    LiveData<List<CategoryModel>> loadCategoryItems();

    @Query("SELECT * FROM category WHERE id = :id")
    LiveData<CategoryModel> fetchCategorybyId (int id);

    @Query("SELECT * FROM category")
    CategoryModel loadCategory();

    @Query("SELECT COUNT(id) FROM category")
    int countCategory();

    @Insert(onConflict = REPLACE)
    void updateCategory (CategoryModel categoryModel);

    @Delete
    void deleteCategory(CategoryModel categoryModel);
}