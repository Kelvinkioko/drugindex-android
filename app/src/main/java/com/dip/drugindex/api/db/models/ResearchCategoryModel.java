package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

@Entity(tableName = "researchcategory")
public class ResearchCategoryModel {

    @NonNull
    @PrimaryKey
    public String id;
    public String category, image;

    public ResearchCategoryModel() { }

    public ResearchCategoryModel(@NonNull String id, String category, String image) {
        this.id = id;
        this.category = category;
        this.image = image;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
