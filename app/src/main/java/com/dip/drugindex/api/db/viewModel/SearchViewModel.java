package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.SearchDao;
import com.dip.drugindex.api.db.models.SearchModel;
import com.dip.drugindex.api.db.models.SearchModel;
import com.dip.drugindex.api.db.repo.SearchRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/20/2018.
 */

public class SearchViewModel extends AndroidViewModel {

    private SearchRepository searchRepository;

    public SearchViewModel (Application application) {
        super(application);
        searchRepository = new SearchRepository(application);
    }

    public LiveData<List<SearchModel>> getAllSearch(){
        return searchRepository.getAllSearchItems();
    }
}