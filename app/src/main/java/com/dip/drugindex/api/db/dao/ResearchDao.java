package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.ResearchModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

@Dao
public interface ResearchDao {

    @Insert
    void insertResearch(ResearchModel researchModel);

    @Query("SELECT * FROM research")
    LiveData<List<ResearchModel>> loadResearchItems();

    @Query("SELECT * FROM research WHERE id = :id")
    LiveData<ResearchModel> fetchResearchbyId (int id);

    @Query("SELECT * FROM research")
    ResearchModel loadResearch();

    @Query("SELECT COUNT(id) FROM research")
    int countResearch();

    @Insert(onConflict = REPLACE)
    void updateResearch (ResearchModel researchModel);

    @Delete
    void deleteResearch(ResearchModel researchModel);
}
