package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ResearchCategoryDao;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchCategoryRepository {

    private final ResearchCategoryDao researchCategoryDao;

    public ResearchCategoryRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        researchCategoryDao = db.researchCategoryDao();
    }

    public void addResearchCategoryItems(ResearchCategoryModel researchCategoryModel){
        researchCategoryDao.insertResearchCategory(researchCategoryModel);
    }

    public LiveData<ResearchCategoryModel> getResearchCategoryItems(int id){
        return researchCategoryDao.fetchResearchCategorybyId(id);
    }

    public LiveData<List<ResearchCategoryModel>> getAllResearchCategoryItems(){
        return researchCategoryDao.loadResearchCategoryItems();
    }

    public void updateResearchCategoryItems(ResearchCategoryModel researchCategoryModel){
        researchCategoryDao.updateResearchCategory(researchCategoryModel);
    }

    public void deleteResearchCategoryItems(ResearchCategoryModel researchCategoryModel){
        researchCategoryDao.deleteResearchCategory(researchCategoryModel);
    }
}