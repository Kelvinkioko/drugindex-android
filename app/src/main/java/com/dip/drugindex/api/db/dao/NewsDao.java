package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.NewsModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

@Dao
public interface NewsDao {

    @Insert
    void insertNews(NewsModel newsModel);

    @Query("SELECT * FROM news")
    LiveData<List<NewsModel>> loadNewsItems();

    @Query("SELECT * FROM news WHERE id = :id")
    LiveData<NewsModel> fetchNewsbyId (int id);

    @Query("SELECT * FROM news")
    NewsModel loadNews();

    @Query("SELECT COUNT(id) FROM news")
    int countNews();

    @Insert(onConflict = REPLACE)
    void updateNews (NewsModel newsModel);

    @Delete
    void deleteNews(NewsModel newsModel);
}
