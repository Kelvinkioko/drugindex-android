package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ManufacturerDao;
import com.dip.drugindex.api.db.models.ManufacturerModel;
import com.dip.drugindex.api.db.models.ManufacturerModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */


public class ManufacturerViewModel extends AndroidViewModel {

    LiveData<List<ManufacturerModel>> liveManufacturer;

    public ManufacturerViewModel (Application application) {
        super(application);
    }

    public LiveData<List<ManufacturerModel>> getAllManufacturer(){
        try {
            liveManufacturer = new fetchManufacturerTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveManufacturer;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchManufacturerTask extends AsyncTask<Void, Void, LiveData<List<ManufacturerModel>>> {
        @Override
        protected LiveData<List<ManufacturerModel>> doInBackground(Void... voids){
            MutableLiveData<List<ManufacturerModel>> tempModel = new MutableLiveData<>();
            List<ManufacturerModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_MANUFACTURER, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONObject jObj = new JSONObject(jsonString);
                    Log.e("Active", jObj.toString());
                    try {
                        JSONArray feedArray = jObj.getJSONArray("manufacturer");

                        if(feedArray.length() > 0){
                            for (int i = 0; i < feedArray.length(); i++) {
                                JSONObject feedObj = (JSONObject) feedArray.get(i);

                                ManufacturerModel item = new ManufacturerModel();
                                item.setManufactureid(feedObj.getString("manufacturer_id"));
                                item.setTitle(feedObj.getString("manufacturers_name"));

                                tempServiceModel.add(item);
                                tempModel.postValue(tempServiceModel);
                            }
                        }else{ }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }
    
}
