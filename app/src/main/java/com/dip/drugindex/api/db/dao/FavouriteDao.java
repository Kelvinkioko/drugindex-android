package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.FavouriteModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 5/10/2018.
 */

@Dao
public interface FavouriteDao {

    @Insert
    void insertFavourite(FavouriteModel favouriteModel);

    @Query("SELECT * FROM favourite")
    LiveData<List<FavouriteModel>> loadFavouriteItems();

    @Query("SELECT * FROM favourite WHERE id = :id")
    LiveData<FavouriteModel> fetchFavouritebyId (int id);

    @Query("SELECT * FROM favourite")
    FavouriteModel loadFavourite();

    @Query("SELECT COUNT(id) FROM favourite")
    int countFavourite();

    @Insert(onConflict = REPLACE)
    void updateFavourite (FavouriteModel favouriteModel);

    @Delete
    void deleteFavourite(FavouriteModel favouriteModel);
}
