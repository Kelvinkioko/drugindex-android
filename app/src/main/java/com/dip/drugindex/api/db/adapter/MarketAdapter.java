package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.core.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

public class MarketAdapter extends RecyclerView.Adapter<MarketAdapter.MyViewHolder> implements Filterable {

    private SessionManager sess;
    private List<MarketModel> dmItems;
    private List<MarketModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<MarketModel> results = new ArrayList<MarketModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final MarketModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<MarketModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView im_id, im_title, im_edition, im_desc, im_cost, im_imageurl, im_createdat;
        public ImageView im_image;

        public MyViewHolder(View view) {
            super(view);
            im_id = view.findViewById(R.id.im_id);
            im_title = view.findViewById(R.id.im_title);
            im_edition = view.findViewById(R.id.im_edition);
            im_desc = view.findViewById(R.id.im_description);
            im_cost = view.findViewById(R.id.im_cost);
            im_imageurl = view.findViewById(R.id.im_imageurl);
            im_createdat = view.findViewById(R.id.im_createdat);

            im_image = view.findViewById(R.id.im_image);

        }
    }

    public MarketAdapter(Context context, List<MarketModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
        sess = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_market, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MarketModel item = dmItems.get(position);

        holder.im_id.setText(String.valueOf(item.getId()));
        holder.im_title.setText(item.getTitle());
        holder.im_edition.setText(item.getEdition());
        holder.im_desc.setText(item.getDescription());
        holder.im_cost.setText(item.getPrice());
        holder.im_imageurl.setText(item.getImage_url());
        holder.im_createdat.setText(item.getCreated_at());

        if (sess.isData()) {
            if (item.getImage_url().isEmpty() || item.getImage_url().equals("null")) {
                holder.im_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            } else {
                Glide.with(context).load(item.getImage_url()).into(holder.im_image);
            }
        }else{
            holder.im_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<MarketModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
