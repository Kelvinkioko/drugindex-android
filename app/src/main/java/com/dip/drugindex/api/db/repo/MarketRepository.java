package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.MarketDao;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.api.db.models.MarketModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */


public class MarketRepository {

    private final MarketDao marketDao;

    public MarketRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        marketDao = db.marketDao();
    }

    public void addMarketItems(MarketModel marketModel){
        marketDao.insertMarket(marketModel);
    }

    public LiveData<MarketModel> getMarketItems(int id){
        return marketDao.fetchMarketbyId(id);
    }

    public LiveData<List<MarketModel>> getAllMarketItems(){
        return marketDao.loadMarketItems();
    }

    public void updateMarketItems(MarketModel marketModel){
        marketDao.updateMarket(marketModel);
    }

    public void deleteMarketItems(MarketModel marketModel){
        marketDao.deleteMarket(marketModel);
    }
}
