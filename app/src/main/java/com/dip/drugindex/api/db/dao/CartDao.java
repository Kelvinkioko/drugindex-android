package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.CartModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/26/2018.
 */

@Dao
public interface CartDao {

    @Insert
    void insertCart(CartModel cartModel);

    @Query("SELECT * FROM cart")
    LiveData<List<CartModel>> loadCartItems();

    @Query("SELECT * FROM cart WHERE id = :id")
    LiveData<CartModel> fetchCartbyId (int id);

    @Query("SELECT * FROM cart")
    CartModel loadCart();

    @Query("SELECT COUNT(id) FROM cart")
    int countCart();

    @Insert(onConflict = REPLACE)
    void updateCart (CartModel cartModel);

    @Delete
    void deleteCart(CartModel cartModel);
}