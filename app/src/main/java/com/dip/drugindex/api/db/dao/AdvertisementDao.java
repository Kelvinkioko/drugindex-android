package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.AdvertisementModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 5/7/2018.
 */
@Dao
public interface AdvertisementDao {

    @Insert
    void insertAdvertisement(AdvertisementModel advertisementModel);

    @Query("SELECT * FROM active")
    LiveData<List<AdvertisementModel>> loadAdvertisementItems();

    @Query("SELECT * FROM advertisement WHERE id = :id")
    LiveData<AdvertisementModel> fetchAdvertisementbyId (int id);

    @Query("SELECT * FROM active")
    AdvertisementModel loadAdvertisement();

    @Query("SELECT COUNT(id) FROM active")
    int countAdvertisement();

    @Insert(onConflict = REPLACE)
    void updateAdvertisement (AdvertisementModel advertisementModel);

    @Delete
    void deleteAdvertisement(AdvertisementModel advertisementModel);
}