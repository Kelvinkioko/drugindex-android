package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.ManufacturerModel;

import java.util.ArrayList;
import java.util.List;

public class ManufacturerAdapter extends RecyclerView.Adapter<ManufacturerAdapter.MyViewHolder> implements Filterable {

    private List<ManufacturerModel> dmItems;
    private List<ManufacturerModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<ManufacturerModel> results = new ArrayList<>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final ManufacturerModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<ManufacturerModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ii_id, ii_title;

        public MyViewHolder(View view) {
            super(view);
            ii_id = view.findViewById(R.id.ii_id);
            ii_title = view.findViewById(R.id.ii_title);
        }
    }

    public ManufacturerAdapter(Context context, List<ManufacturerModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public ManufacturerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_index, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ManufacturerAdapter.MyViewHolder holder, int position) {
        ManufacturerModel item = dmItems.get(position);

        holder.ii_id.setText(item.getManufactureid());
        holder.ii_title.setText(item.getTitle());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<ManufacturerModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}