package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.IndexModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.MyViewHolder> implements Filterable {

    private List<IndexModel> dmItems;
    private List<IndexModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<IndexModel> results = new ArrayList<>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final IndexModel cd: orig){
                            if(cd.getTitle().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<IndexModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ii_id, ii_title;

        public MyViewHolder(View view) {
            super(view);
            ii_id = view.findViewById(R.id.ii_id);
            ii_title = view.findViewById(R.id.ii_title);
        }
    }

    public IndexAdapter(Context context, List<IndexModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_index, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IndexModel item = dmItems.get(position);

        holder.ii_id.setText(item.getId());
        holder.ii_title.setText(item.getTitle());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }
}
