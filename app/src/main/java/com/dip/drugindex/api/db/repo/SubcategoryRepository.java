package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.SubcategoryDao;
import com.dip.drugindex.api.db.models.SubcategoryModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

public class SubcategoryRepository {

    private final SubcategoryDao subcategoryDao;

    public SubcategoryRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        subcategoryDao = db.subcategoryDao();
    }

    public void addSubcategoryItems(SubcategoryModel subcategoryModel){
        subcategoryDao.insertSubcategory(subcategoryModel);
    }

    public LiveData<SubcategoryModel> getSubcategoryItems(int id){
        return subcategoryDao.fetchSubcategorybyId(id);
    }

    public LiveData<List<SubcategoryModel>> getAllSubcategoryItems(){
        return subcategoryDao.loadSubcategoryItems();
    }

    public void updateSubcategoryItems(SubcategoryModel subcategoryModel){
        subcategoryDao.updateSubcategory(subcategoryModel);
    }

    public void deleteSubcategoryItems(SubcategoryModel subcategoryModel){
        subcategoryDao.deleteSubcategory(subcategoryModel);
    }
}
