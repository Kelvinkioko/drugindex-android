package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

@Entity(tableName = "newscategory")
public class NewsCategoryModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String category_name, image_url;

    public NewsCategoryModel() {}

    public NewsCategoryModel(@NonNull int id, String category_name, String image_url) {
        this.id = id;
        this.category_name = category_name;
        this.image_url = image_url;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
