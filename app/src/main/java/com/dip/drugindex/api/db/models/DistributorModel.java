package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Entity(tableName = "distributor")
public class DistributorModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String distributorid,  title;

    public DistributorModel() {}

    public DistributorModel(@NonNull int id, String distributorid, String title) {
        this.id = id;
        this.distributorid = distributorid;
        this.title = title;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getDistributorid() {
        return distributorid;
    }

    public void setDistributorid(String distributorid) {
        this.distributorid = distributorid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
