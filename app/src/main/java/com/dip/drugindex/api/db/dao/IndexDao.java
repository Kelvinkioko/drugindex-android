package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.IndexModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

@Dao
public interface IndexDao {

    @Insert
    void insertIndex(IndexModel IndexModel);

    @Query("SELECT * FROM index")
    LiveData<List<IndexModel>> loadIndexItems();

    @Query("SELECT * FROM Index WHERE id = :id")
    LiveData<IndexModel> fetchIndexbyId (int id);

    @Query("SELECT * FROM Index")
    IndexModel loadIndex();

    @Query("SELECT COUNT(id) FROM Index")
    int countIndex();

    @Insert(onConflict = REPLACE)
    void updateIndex (IndexModel IndexModel);

    @Delete
    void deleteIndex(IndexModel IndexModel);
}
