package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.NewsCategoryModel;
import com.dip.drugindex.core.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsCatAdapter extends RecyclerView.Adapter<NewsCatAdapter.MyViewHolder> implements Filterable {

    private SessionManager sess;
    private List<NewsCategoryModel> dmItems;
    private List<NewsCategoryModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<NewsCategoryModel> results = new ArrayList<NewsCategoryModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final NewsCategoryModel cd: orig){
                            if(cd.getCategory_name().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<NewsCategoryModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView inc_id, inc_category;
        public ImageView inc_image;

        public MyViewHolder(View view) {
            super(view);
            inc_category = view.findViewById(R.id.inc_category);
            inc_id = view.findViewById(R.id.inc_id);
            inc_image = view.findViewById(R.id.inc_picture);

        }
    }

    public NewsCatAdapter(Context context, List<NewsCategoryModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
        this.sess = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsCategoryModel item = dmItems.get(position);

        holder.inc_id.setText(String.valueOf(item.getId()));
        holder.inc_category.setText(item.getCategory_name());

        if(sess.isData()) {
            if (!item.getImage_url().isEmpty() && !item.getImage_url().equals("null")) {
                Glide.with(context).load(item.getImage_url()).into(holder.inc_image);
            } else {
                holder.inc_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            }
        }else{
            holder.inc_image.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<NewsCategoryModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
