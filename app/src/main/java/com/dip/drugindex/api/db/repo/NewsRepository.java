package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.NewsDao;
import com.dip.drugindex.api.db.models.NewsModel;
import com.dip.drugindex.api.db.models.NewsModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/17/2018.
 */

public class NewsRepository {

    private final NewsDao newsDao;

    public NewsRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        newsDao = db.newsDao();
    }

    public void addNewsItems(NewsModel newsModel){
        newsDao.insertNews(newsModel);
    }

    public LiveData<NewsModel> getNewsItems(int id){
        return newsDao.fetchNewsbyId(id);
    }

    public LiveData<List<NewsModel>> getAllNewsItems(){
        return newsDao.loadNewsItems();
    }

    public void updateNewsItems(NewsModel newsModel){
        newsDao.updateNews(newsModel);
    }

    public void deleteNewsItems(NewsModel newsModel){
        newsDao.deleteNews(newsModel);
    }
}
