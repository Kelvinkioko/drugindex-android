package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

@Entity(tableName = "research")
public class ResearchModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String category_name, title, published_by, url, image_url, page_count, topics, price, date_published;

    public ResearchModel() {
    }

    public ResearchModel(@NonNull int id, String category_name, String title, String published_by, String url,
                         String image_url, String page_count, String topics, String price, String date_published) {
        this.id = id;
        this.category_name = category_name;
        this.title = title;
        this.published_by = published_by;
        this.url = url;
        this.image_url = image_url;
        this.page_count = page_count;
        this.topics = topics;
        this.price = price;
        this.date_published = date_published;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublished_by() {
        return published_by;
    }

    public void setPublished_by(String published_by) {
        this.published_by = published_by;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getPage_count() {
        return page_count;
    }

    public void setPage_count(String page_count) {
        this.page_count = page_count;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate_published() {
        return date_published;
    }

    public void setDate_published(String date_published) {
        this.date_published = date_published;
    }
}
