package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ManufacturerDao;
import com.dip.drugindex.api.db.models.ManufacturerModel;
import com.dip.drugindex.api.db.models.ManufacturerModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */


public class ManufacturerRepository {

    private final ManufacturerDao manufacturerDao;

    public ManufacturerRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        manufacturerDao = db.manufacturerDao();
    }

    public void addManufacturerItems(ManufacturerModel manufacturerModel){
        manufacturerDao.insertManufacturer(manufacturerModel);
    }

    public LiveData<ManufacturerModel> getManufacturerItems(int id){
        return manufacturerDao.fetchManufacturerbyId(id);
    }

    public LiveData<List<ManufacturerModel>> getAllManufacturerItems(){
        return manufacturerDao.loadManufacturerItems();
    }

    public void updateManufacturerItems(ManufacturerModel manufacturerModel){
        manufacturerDao.updateManufacturer(manufacturerModel);
    }

    public void deleteManufacturerItems(ManufacturerModel manufacturerModel){
        manufacturerDao.deleteManufacturer(manufacturerModel);
    }
}
