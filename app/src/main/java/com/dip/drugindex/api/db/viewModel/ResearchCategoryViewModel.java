package com.dip.drugindex.api.db.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ResearchCategoryDao;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;
import com.dip.drugindex.api.db.models.ResearchCategoryModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.UrlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 3/12/2018.
 */

public class ResearchCategoryViewModel extends AndroidViewModel {

    LiveData<List<ResearchCategoryModel>> liveResearchCategory;

    public ResearchCategoryViewModel (Application application) {
        super(application);
    }

    public LiveData<List<ResearchCategoryModel>> getAllResearchCategory(){
        try {
            liveResearchCategory = new fetchResearchCategoryTask().execute().get();
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
        }
        return liveResearchCategory;
    }

    //    AsyncTask Methods to manage data entry and edits
    private class fetchResearchCategoryTask extends AsyncTask<Void, Void, LiveData<List<ResearchCategoryModel>>> {
        @Override
        protected LiveData<List<ResearchCategoryModel>> doInBackground(Void... voids){
            MutableLiveData<List<ResearchCategoryModel>> tempModel = new MutableLiveData<>();
            List<ResearchCategoryModel> tempServiceModel = new ArrayList<>();

            IndexCache cacheRequest = new IndexCache(Request.Method.GET, UrlConfig.URL_RESEARCH_CATEGORY, response -> {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));

                    JSONObject jObj = new JSONObject(jsonString);
                    JSONArray feedArray = jObj.getJSONArray("research_category");

                    if(feedArray.length() > 0){
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);

                            ResearchCategoryModel item = new ResearchCategoryModel();;
                            item.setId(feedObj.getString("id"));
                            item.setCategory(feedObj.getString("category_name"));
                            item.setImage(feedObj.getString("image_url"));

                            tempServiceModel.add(item);
                            tempModel.postValue(tempServiceModel);
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }, error -> { });

            Volley.newRequestQueue(getApplication().getApplicationContext()).add(cacheRequest);

            return tempModel;
        }
    }
}