package com.dip.drugindex.api.db.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.base.DrugDatabase;
import com.dip.drugindex.api.db.dao.ActiveDao;
import com.dip.drugindex.api.db.models.ActiveModel;
import com.dip.drugindex.api.db.models.ActiveModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class ActiveRepository {

    private final ActiveDao activeDao;

    public ActiveRepository(Application application){
        DrugDatabase db = DrugDatabase.getAppDatabase(application);
        activeDao = db.activeDao();
    }

    public void addActiveItems(ActiveModel activeModel){
        activeDao.insertActive(activeModel);
    }

    public LiveData<ActiveModel> getActiveItems(int id){
        return activeDao.fetchActivebyId(id);
    }

    public LiveData<List<ActiveModel>> getAllActiveItems(){
        return activeDao.loadActiveItems();
    }

    public void updateActiveItems(ActiveModel activeModel){
        activeDao.updateActive(activeModel);
    }

    public void deleteActiveItems(ActiveModel activeModel){
        activeDao.deleteActive(activeModel);
    }
}