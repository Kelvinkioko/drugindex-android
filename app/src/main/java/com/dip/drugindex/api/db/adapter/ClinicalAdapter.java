package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.ClinicalModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/30/2018.
 */

public class ClinicalAdapter extends RecyclerView.Adapter<ClinicalAdapter.MyViewHolder> implements Filterable {

    private List<ClinicalModel> dmItems;
    private List<ClinicalModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<ClinicalModel> results = new ArrayList<ClinicalModel>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final ClinicalModel cd: orig){
                            if(cd.getName().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<ClinicalModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ic_id, ic_name;

        public MyViewHolder(View view) {
            super(view);
            ic_id = view.findViewById(R.id.ic_id);
            ic_name = view.findViewById(R.id.ic_drug);
        }
    }

    public ClinicalAdapter(Context context, List<ClinicalModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clinical, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ClinicalModel item = dmItems.get(position);

        holder.ic_id.setText(item.getId());
        holder.ic_name.setText(item.getName());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<ClinicalModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
