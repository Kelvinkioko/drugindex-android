package com.dip.drugindex.api.db.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.api.db.models.AdvertisementModel;
import com.dip.drugindex.api.db.models.DrugModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvin Kioko on 3/8/2018.
 */

public class DrugAdapter extends RecyclerView.Adapter<DrugAdapter.MyViewHolder> implements Filterable {

    private List<DrugModel> dmItems;
    private List<DrugModel> orig;
    public Context context;

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<DrugModel> results = new ArrayList<>();
                if(orig == null)
                    orig = dmItems;
                if(constraint != null){
                    if(orig != null && orig.size()>0){
                        for(final DrugModel cd: orig){
                            if(cd.getName().toLowerCase().contains(constraint.toString()))results.add(cd);
                        }
                    }
                    oReturn.values = results;
                }

                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results){
                dmItems = (ArrayList<DrugModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView id_id, id_name, id_manufacturer;

        public MyViewHolder(View view) {
            super(view);
            id_id = view.findViewById(R.id.id_id);
            id_name = view.findViewById(R.id.id_drug);
            id_manufacturer = view.findViewById(R.id.id_manufacturer);
        }
    }

    public DrugAdapter(Context context, List<DrugModel> dmItems) {
        this.context = context;
        this.dmItems = dmItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_drug, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DrugModel item = dmItems.get(position);

        holder.id_id.setText(item.getDrug_id());
        holder.id_name.setText(item.getName());
        holder.id_manufacturer.setText(item.getManufacturer());

    }

    @Override
    public int getItemCount() {
        return dmItems.size();
    }

    public void setItems(List<DrugModel> apps){
        dmItems = apps;
        notifyDataSetChanged();
    }
}
