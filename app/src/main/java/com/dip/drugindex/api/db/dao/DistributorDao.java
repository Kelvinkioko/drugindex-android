package com.dip.drugindex.api.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.dip.drugindex.api.db.models.DistributorModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Kelvin Kioko on 4/15/2018.
 */

@Dao
public interface DistributorDao {

    @Insert
    void insertDistributor(DistributorModel distributorModel);

    @Query("SELECT * FROM distributor")
    LiveData<List<DistributorModel>> loadDistributorItems();

    @Query("SELECT * FROM distributor WHERE id = :id")
    LiveData<DistributorModel> fetchDistributorbyId (int id);

    @Query("SELECT * FROM distributor")
    DistributorModel loadDistributor();

    @Query("SELECT COUNT(id) FROM distributor")
    int countDistributor();

    @Insert(onConflict = REPLACE)
    void updateDistributor (DistributorModel distributorModel);

    @Delete
    void deleteDistributor(DistributorModel distributorModel);
}
