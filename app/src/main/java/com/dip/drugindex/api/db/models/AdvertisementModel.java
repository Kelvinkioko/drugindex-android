package com.dip.drugindex.api.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Kelvin Kioko on 5/7/2018.
 */

@Entity(tableName = "advertisement")
public class AdvertisementModel {

    @NonNull
    @PrimaryKey
    public int id;
    public String title, edition, price, image_url, created_at, web_url;

    public AdvertisementModel() {}

    public AdvertisementModel(@NonNull int id, String title, String edition, String price, String image_url, String created_at, String web_url) {
        this.id = id;
        this.title = title;
        this.edition = edition;
        this.price = price;
        this.image_url = image_url;
        this.created_at = created_at;
        this.web_url = web_url;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }
}