package com.dip.drugindex.views.text;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by foram on 20/2/17.
 */

public class TextView_Semibold extends AppCompatTextView {

    public TextView_Semibold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextView_Semibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextView_Semibold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/semibold.OTF");
            setTypeface(tf);
        }
    }

}
