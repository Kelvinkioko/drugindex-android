package com.dip.drugindex.views.pdfView.adapter;

import android.graphics.Bitmap;

public interface BitmapContainer {
    Bitmap get(int position);

    void remove(int position);

    void clear();
}