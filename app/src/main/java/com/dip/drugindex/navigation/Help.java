package com.dip.drugindex.navigation;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.Walkthrough;
import com.dip.drugindex.WelcomeActivity;
import com.dip.drugindex.ui.help.HelpAbout;
import com.dip.drugindex.ui.help.HelpFaq;
import com.dip.drugindex.ui.help.HelpReport;
import com.dip.drugindex.ui.help.HelpTerms;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Help extends Fragment {

    @BindView(R.id.help_faq) TextView help_faq;
    @BindView(R.id.help_about) TextView help_about;
    @BindView(R.id.help_report) TextView help_report;
    @BindView(R.id.help_terms) TextView help_terms;
    @BindView(R.id.help_walk) TextView help_walk;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tb_text) TextView tb_text;

    Dialog myDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_help, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Help");

        DrawerLayout drawer = Help.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        myDialog = new Dialog(getActivity());

        help_faq.setOnClickListener(v->startActivity(new Intent(getActivity(), HelpFaq.class)));

        help_about.setOnClickListener(v->ShowPopup());

        help_report.setOnClickListener(v->startActivity(new Intent(getActivity(), HelpReport.class)));

        help_terms.setOnClickListener(v->startActivity(new Intent(getActivity(), HelpTerms.class)));

        help_walk.setOnClickListener(v->startActivity(new Intent(getActivity(), Walkthrough.class)));

        return x;
    }

    public void ShowPopup() {
        myDialog.setContentView(R.layout.help_about);
//        txtclose =(TextView) myDialog.findViewById(R.id.txtclose);
//        txtclose.setText("M");
//        btnFollow = (Button) myDialog.findViewById(R.id.btnfollow);
//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }
}
