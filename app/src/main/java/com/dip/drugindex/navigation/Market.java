package com.dip.drugindex.navigation;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ClinicalAdapter;
import com.dip.drugindex.api.db.adapter.MarketAdapter;
import com.dip.drugindex.api.db.models.MarketModel;
import com.dip.drugindex.api.db.viewModel.ClinicalViewModel;
import com.dip.drugindex.api.db.viewModel.MarketViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.cart.ActivityCart;
import com.dip.drugindex.ui.details.MarketDetails;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Market extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVmarket) RecyclerView RVmarket;
    @BindView(R.id.mar_empty)ImageView empty;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tb_text) TextView tb_text;

    @BindView(R.id.search_view) MaterialSearchView searchView;

    private MarketAdapter marketAdapter;
    private List<MarketModel> marketModel;
    private MarketViewModel marketViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        marketViewModel = ViewModelProviders.of(this).get(MarketViewModel.class);

        if(marketModel.size() == 0){
            RVmarket.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        marketViewModel.getAllMarket().observe(this, marketModels -> {
            if(marketModels.size() > 0){
                if (marketModel.size() == 0 ||
                        marketModel.size() > marketModels.size() ||
                        marketModel.size() < marketModels.size()) {

                    RVmarket.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    marketModel.clear();
                    marketModel = marketModels;
                    marketAdapter.setItems(marketModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_market, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Market");

        DrawerLayout drawer = Market.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVmarket.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVmarket, (view1, position) -> {

            TextView im_id = view1.findViewById(R.id.im_id);
            String str_id = im_id.getText().toString().trim();
            TextView im_title = view1.findViewById(R.id.im_title);
            String str_title = im_title.getText().toString().trim();
            TextView im_edition = view1.findViewById(R.id.im_edition);
            String str_edition = im_edition.getText().toString().trim();
            TextView im_desc = view1.findViewById(R.id.im_description);
            String str_desc = im_desc.getText().toString().trim();
            TextView im_cost = view1.findViewById(R.id.im_cost);
            String str_cost = im_cost.getText().toString().trim();
            TextView im_imageurl = view1.findViewById(R.id.im_imageurl);
            String str_imageurl = im_imageurl.getText().toString().trim();
            TextView im_createdat = view1.findViewById(R.id.im_createdat);
            String str_createdat = im_createdat.getText().toString().trim();

            Intent active = new Intent(getActivity(), MarketDetails.class);
            active.putExtra("id", str_id);
            active.putExtra("title", str_title);
            active.putExtra("edition", str_edition);
            active.putExtra("desc", str_desc);
            active.putExtra("cost", str_cost);
            active.putExtra("imageurl", str_imageurl);
            active.putExtra("createdat", str_createdat);
            startActivity(active);
        }));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextChange", query);
                if(TextUtils.isEmpty(query)){
                    marketAdapter.getFilter().filter("");
                }else{
                    marketAdapter.getFilter().filter(query.toString());
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextSubmit", newText);
                if(TextUtils.isEmpty(newText)){
                    marketAdapter.getFilter().filter("");
                }else{
                    marketAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        marketModel = new ArrayList<>();

        marketAdapter = new MarketAdapter(getActivity(), marketModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVmarket.setLayoutManager(mLayoutManager);
        RVmarket.setItemAnimator(new DefaultItemAnimator());
        RVmarket.setAdapter(marketAdapter);

    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_market, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView.setMenuItem(searchItem);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cart:
                    startActivity(new Intent(getActivity(), ActivityCart.class));
                    getActivity().overridePendingTransition(R.anim.dialog_show, R.anim.dialog_dismiss);
                return true;
        }

        return false;
    }
}
