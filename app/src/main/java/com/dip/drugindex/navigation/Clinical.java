package com.dip.drugindex.navigation;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.dip.drugindex.R;
import com.dip.drugindex.api.db.adapter.ClinicalAdapter;
import com.dip.drugindex.api.db.models.ClinicalModel;
import com.dip.drugindex.api.db.viewModel.ActiveViewModel;
import com.dip.drugindex.api.db.viewModel.ClinicalViewModel;
import com.dip.drugindex.api.server.IndexCache;
import com.dip.drugindex.api.server.RecyclerTouchListener;
import com.dip.drugindex.api.server.UrlConfig;
import com.dip.drugindex.ui.details.ClinicalDetails;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Clinical extends Fragment {

    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipecontainer;
    @BindView(R.id.RVreference) RecyclerView RVrefence;
    @BindView(R.id.nr_empty)ImageView empty;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.tb_text) TextView tb_text;

    private ClinicalAdapter clinicalAdapter;
    private List<ClinicalModel> clinicalModel;
    private String[] suggestions;

    private ClinicalViewModel clinicalViewModel;

    private int search = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        clinicalViewModel = ViewModelProviders.of(this).get(ClinicalViewModel.class);

        if(clinicalModel.size() == 0){
            RVrefence.setVisibility(GONE);
            empty.setVisibility(VISIBLE);
        }

        clinicalViewModel.getAllClinical().observe(this, clinicalModels -> {
            if(clinicalModels.size() > 0){
                if (clinicalModel.size() == 0 ||
                        clinicalModel.size() > clinicalModels.size() ||
                        clinicalModel.size() < clinicalModels.size()) {

                    RVrefence.setVisibility(VISIBLE);
                    empty.setVisibility(GONE);

                    clinicalModel = clinicalModels;
                    clinicalAdapter.setItems(clinicalModel);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_reference, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Clinical Reference");

        DrawerLayout drawer = Clinical.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Configure the refreshing colors
        swipecontainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RVrefence.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), RVrefence, (view1, position) -> {
            TextView ia_id = view1.findViewById(R.id.ic_id);
            String str_id = ia_id.getText().toString().trim();
            TextView ia_drug = view1.findViewById(R.id.ic_drug);
            String str_drug = ia_drug.getText().toString().trim();

            Intent clinical = new Intent(getActivity(), ClinicalDetails.class);
            clinical.putExtra("id", str_id);
            clinical.putExtra("drug", str_drug);
            startActivity(clinical);
        }));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextChange", query);
                if(TextUtils.isEmpty(query)){
                    clinicalAdapter.getFilter().filter("");
                }else{
                    clinicalAdapter.getFilter().filter(query.toString());
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextSubmit", newText);
                if(TextUtils.isEmpty(newText)){
                    clinicalAdapter.getFilter().filter("");
                }else{
                    clinicalAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clinicalModel = new ArrayList<>();

        clinicalAdapter = new ClinicalAdapter(getActivity(), clinicalModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RVrefence.setLayoutManager(mLayoutManager);
        RVrefence.setItemAnimator(new DefaultItemAnimator());
        RVrefence.setAdapter(clinicalAdapter);

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView.setMenuItem(searchItem);
        searchView.setSuggestions(suggestions);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
