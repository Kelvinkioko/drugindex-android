package com.dip.drugindex.navigation;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.ui.drugindex.DIActive;
import com.dip.drugindex.ui.drugindex.DIBrand;
import com.dip.drugindex.ui.drugindex.DICategory;
import com.dip.drugindex.ui.drugindex.DIDistributor;
import com.dip.drugindex.ui.drugindex.DIManufacturer;
import com.dip.drugindex.ui.drugindex.DISub;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Index extends Fragment {

    @BindView(R.id.ni_tabs) TabLayout tabLayout;
    @BindView(R.id.ni_pager) ViewPager viewPager;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tb_text) TextView tb_text;

    public static int int_items = 6;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_index, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Drug Index");

        DrawerLayout drawer = Index.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));

        for (int i = 0; i < tabLayout.getTabCount(); i++){
            TextView tv = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_textview, null);
            tv.setTypeface(tf);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        return x;
    }



    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {super(fm);}

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new DIActive();
                case 1:
                    return new DIBrand();
                case 2:
                    return new DICategory();
                case 3:
                    return new DISub();
                case 4:
                    return new DIManufacturer();
                case 5:
                    return new DIDistributor();
            }
            return null;
        }

        @Override
        public int getCount() {return int_items;}

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "ACTIVE ING.";
                case 1:
                    return "BRAND";
                case 2:
                    return "CATEGORY";
                case 3:
                    return "SUBCATEGORY";
                case 4:
                    return "MANUFACTURER";
                case 5:
                    return "DISTRIBUTOR";
            }
            return null;
        }
    }
}
