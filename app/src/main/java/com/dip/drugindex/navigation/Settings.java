package com.dip.drugindex.navigation;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.WelcomeActivity;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.core.SessionManager;
import com.dip.drugindex.ui.profile.ProfileUpdate;
import com.dip.drugindex.ui.setts.PasswordActivity;
import com.dip.drugindex.ui.setts.PaymentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Settings extends Fragment {

    @BindView(R.id.ns_personal) LinearLayout ns_personal;
    @BindView(R.id.ns_password) LinearLayout ns_password;
    @BindView(R.id.ns_logout) TextView ns_logout;

    @BindView(R.id.gen_data) CheckBox data;
    @BindView(R.id.gen_notification) CheckBox notification;

    @BindView(R.id.gen_data_lay) LinearLayout data_lay;
    @BindView(R.id.gen_notification_lay) LinearLayout notification_lay;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tb_text) TextView tb_text;

    private SessionManager sess;
    private SQLiteHandler db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_settings, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Settings");

        DrawerLayout drawer = Settings.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        sess = new SessionManager(getActivity());
        db = new SQLiteHandler(getActivity());

        ns_logout.setOnClickListener(v -> {
            db.deleteSearch();
            db.deleteUsers();
            sess.setLogin(false);
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
            getFragmentManager().popBackStackImmediate();
        });

        ns_password.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), PasswordActivity.class));
        });

        if(sess.isData()){
            data.setChecked(true);
        }else{
            data.setChecked(false);
        }

        if (sess.isNotify()){
            notification.setChecked(true);
        }else{
            notification.setChecked(false);
        }

        ns_personal.setOnClickListener(v -> startActivity(new Intent(getActivity(), ProfileUpdate.class)));

//        data.setOnClickListener(v -> {
//            if(data.isChecked()){
//                data.setChecked(false); sess.setData(false);
//            }else{
//                data.setChecked(true);  sess.setData(true);
//            }
//        });

        data_lay.setOnClickListener(v->{
            if(data.isChecked()){
                data.setChecked(false); sess.setData(false);
            }else{
                data.setChecked(true);  sess.setData(true);
            }
        });

//        notification.setOnClickListener(v->{
//            if(notification.isChecked()){
//                notification.setChecked(false); sess.setNotification(false);
//            }else{
//                notification.setChecked(true);  sess.setNotification(true);
//            }
//        });

        notification_lay.setOnClickListener(v->{
            if(notification.isChecked()){
                notification.setChecked(false); sess.setNotification(false);
            }else{
                notification.setChecked(true);  sess.setNotification(true);
            }
        });

        return x;
    }
}
