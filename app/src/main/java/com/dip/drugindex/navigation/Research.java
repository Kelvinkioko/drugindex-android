package com.dip.drugindex.navigation;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dip.drugindex.R;
import com.dip.drugindex.ui.researchdb.ResearchCategory;
import com.dip.drugindex.ui.researchdb.ResearchRecent;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kelvin Kioko on 2/26/2018.
 */

public class Research extends Fragment {

    @BindView(R.id.nr_tabs) TabLayout tabLayout;
    @BindView(R.id.nr_pager) ViewPager viewPager;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tb_text) TextView tb_text;

    public static int int_items = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.nav_research, null);
        ButterKnife.bind(this, x);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        Typeface tf =   Typeface.createFromAsset(getActivity().getAssets(),"fonts/regular.OTF");
        tb_text.setTypeface(tf);
        tb_text.setText("Research Database");

        DrawerLayout drawer = Research.this.getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));

        for (int i = 0; i < tabLayout.getTabCount(); i++){
            TextView tv = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_textview, null);
            tv.setTypeface(tf);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        return x;
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {super(fm);}

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ResearchRecent();
                case 1:
                    return new ResearchCategory();
            }
            return null;
        }

        @Override
        public int getCount() {return int_items;}

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "RECENT";
                case 1:
                    return "CATEGORY";
            }
            return null;
        }
    }
}
