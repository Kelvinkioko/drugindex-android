package com.dip.drugindex;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dip.drugindex.authentication.SigninActivity;
import com.dip.drugindex.core.SQLiteHandler;
import com.dip.drugindex.core.SessionManager;
import com.dip.drugindex.intro.SlideActivity;
import com.dip.drugindex.navigation.Clinical;
import com.dip.drugindex.navigation.Help;
import com.dip.drugindex.navigation.Home;
import com.dip.drugindex.navigation.Index;
import com.dip.drugindex.navigation.Market;
import com.dip.drugindex.navigation.News;
import com.dip.drugindex.navigation.Research;
import com.dip.drugindex.navigation.Settings;
import com.dip.drugindex.ui.profile.ProfileActivity;
import com.dip.drugindex.utils.blurview.BlurImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    public int count  = 0;

    private SQLiteHandler db;
    private SessionManager session;

    HashMap<String, String> user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new SQLiteHandler(this);
        user = db.getUserDetails();

        setProfilenav();

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        mFragmentTransaction.replace(R.id.containerView,new Home()).commit();

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        // session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (!session.isLoggedIn()) {
            startActivity(new Intent(MainActivity.this, SlideActivity.class));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(count == 1)
            {
                count=0;
                super.onBackPressed();
                finish();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Press Back again to quit.", Toast.LENGTH_SHORT).show();
                count++;
            }
        }
    }

    public void setProfilenav(){
        String name = user.get("username");
        String Picc = user.get("profilepicture");

        final NavigationView naView = findViewById(R.id.navigation_view);
        final View header = naView.inflateHeaderView(R.layout.header);

        BlurImageView navBlur = header.findViewById(R.id.navblur);
        TextView navname = header.findViewById(R.id.navname);
        CircleImageView navPicture = header.findViewById(R.id.navpicture);
        LinearLayout navProfile = header.findViewById(R.id.nav_prof);

        navBlur.setProgressBarBgColor(Color.parseColor("#E29C45"));
        navBlur.setProgressBarColor(Color.parseColor("#7BCFA6"));

        String small = "http://upload-images.jianshu.io/upload_images/1825662-4c4e9bc7148749b7.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/62";
        String large = "http://upload-images.jianshu.io/upload_images/1825662-4c4e9bc7148749b7.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/620";

        int blurFactor = BlurImageView.DEFAULT_BLUR_FACTOR;
        navBlur.setBlurFactor(blurFactor);
        navBlur.setFullImageByUrl(small, large);

        if(!Picc.isEmpty() && !Picc.equals("null")) {
            Glide.with(MainActivity.this).load(user.get("profilepicture")).into(navPicture);
        }else{
            navPicture.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
        }

        navProfile.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        });

        navname.setText(name);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Home()).commit();
        } else if (id == R.id.nav_index) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Index()).commit();
        }  else if (id == R.id.nav_clinical) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Clinical()).commit();
        }  else if (id == R.id.nav_research) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Research()).commit();
        } else if (id == R.id.nav_market) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Market()).commit();
        } else if (id == R.id.nav_news) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new News()).commit();
        } else if (id == R.id.nav_help) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Help()).commit();
        } else if (id == R.id.nav_settings) {
            count=0;
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            mFragmentTransaction.replace(R.id.containerView,new Settings()).commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
